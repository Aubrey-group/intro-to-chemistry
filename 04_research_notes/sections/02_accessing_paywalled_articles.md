# Accessing books and articles at UT

**Word count:** <span id="word-count">0</span> words<br>**Reading Time:** <span id="read-time">0</span> minutes

*How to access books/articles by navigating paywalls*

1. Just click the link and see if you can open the pdf document on the other side. If you are on UT's wifi network it will often work even if the article isn't open access. 

2. Connect to UT network and try again. The easiest way to do this is with an [EZProxy bookmarklet](https://guides.lib.utexas.edu/c.php?g=990194&p=7163277). I use this so often I have a keyboard shortcut set to this bookmark in my browser. 

3. Just because you can't access it through EXProxy or the publisher website doesn't mean UT doesn't have access to it. Search for it on the [library's website](https://www.lib.utexas.edu/). The search here is not as good as Google and you have to be a bit more thoughtful about how you type it in. Start with the title and add more info if needs or put important phrases in quotations to narrow results. Again be careful with punctuation marks and chemical formulas. Search engine often don't interpret these reliably.

4. If the library doesn't have it then you might be able to get it through an [Interlibrary loan](https://www.lib.utexas.edu/find-borrow-request/interlibrary-loan).  These requests can take a few days to fulfill.

5. Ask a librarian for guidance.

## When you run out of easy options

1. If the author is still alive you may be able to email them for help accessing their work for suggestions on where to find good resources in their field. In journal articles the corresponding authors email address is provided. But more up to date contact info can often be found by just searching for them on Google.

2. Out of options? Ask an expert in the field for help. They may have access to additional resources (friends at other universities, private libraries, better library skills...).
