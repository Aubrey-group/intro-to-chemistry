# 04 | Literature Research in Chemistry

There are a number of key literature resources and research tools available to aid you in research that broadly fall into two categories in the sciences: (1) the academic literature and (2) data, analysis, and theory. Great strides have been taken to consolidate and codify findings from the primary research into standardized data formats for easy access and bulks analysis. Summaries of a few of the key resources available to get you started include:

## Academic Literature

1. [Google Scholar](https://scholar.google.com/)
2. [Web of Science](https://www.webofscience.com/wos/woscc/basic-search)
3. [UT Austin Library](https://www.lib.utexas.edu/)
4. [Zotero](https://www.zotero.org/)

## Data, Analysis, and Theory

1. [ChemDraw](https://www.perkinelmer.com/category/chemdraw)
2. [Avogadro](https://avogadro.cc/)
3. [Vesta](https://jp-minerals.org/vesta/en/)
4. [Mercury](https://www.ccdc.cam.ac.uk/mercury/)
5. [Materials Project](https://materialsproject.org/)
6. [Protein Data Bank](https://www.rcsb.org/)
7. [Cambridge Structural Database](https://www.ccdc.cam.ac.uk/)