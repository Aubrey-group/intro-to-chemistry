import os
import filecmp
import shutil

def sync_files(static_dir, build_dir, extensions):
    # Ensure the build directory exists
    if not os.path.exists(build_dir):
        os.makedirs(build_dir)

    # Iterate through all files in the static directory
    for filename in os.listdir(static_dir):
        if any(filename.endswith(ext) for ext in extensions):
            static_file = os.path.join(static_dir, filename)
            build_file = os.path.join(build_dir, filename)

            # Check if the file exists in the build directory and if it is different
            if not os.path.exists(build_file) or not filecmp.cmp(static_file, build_file, shallow=False):
                # Copy the file from static to build directory
                shutil.copy2(static_file, build_file)
                print(f"Copied {filename} to {build_dir}")


if __name__ == "__main__":
    static_dir = "_static"
    build_dir = "_build/html/_static"
    extensions = ['.json', '.css', 'js']
    sync_files(static_dir, build_dir, extensions)
    print("Sync _static complete")