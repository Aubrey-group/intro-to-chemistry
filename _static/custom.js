

function count_words() {
  var profile_values = document.getElementsByTagName("section");
  var total_words = 0;
  for (i = 0; i < profile_values.length; i++) {
    total_words += profile_values[i].textContent.trim().split(/\s+/).length;
  }
  document.getElementById("word-count").innerHTML = Math.round(total_words/10)*10;
  document.getElementById("read-time").innerHTML = Math.round(total_words/130)+1;
}

window.onload = count_words;



// Flash cards
document.addEventListener('DOMContentLoaded', function() {
    const flashcardElements = document.querySelectorAll('.flashcard');
    flashcardElements.forEach((flashcardElement) => {
        const filePath = flashcardElement.getAttribute('data-file');
        console.log(filePath);
        let flashcards = [];
        let currentIndex = 0;
        let showAnswer = false;

        fetch(filePath)
            .then(response => response.json())
            .then(data => {
                flashcards = data;
                shuffleArray(flashcards);
                displayCard();
            })
            .catch(error => {
                console.error('Error loading the flashcards:', error);
            });


        function displayCard() {
            const card = flashcards[currentIndex];
            const content = showAnswer ? card.answer : card.question;
            const contentElement = flashcardElement.querySelector('.content');
            contentElement.innerHTML = content;
            console.log('Content before MathJax:', contentElement.innerHTML);

            MathJax.typesetPromise([contentElement])
                .then(() => {
                    console.log('MathJax typeset complete');
                    console.log('Content after MathJax:', contentElement.innerHTML);
                })
                .catch((err) => console.log('MathJax typeset error:', err.message));

        }

        function shuffleArray(array) {
            for (let i = array.length - 1; i > 0; i--) {
                const j = Math.floor(Math.random() * (i + 1));
                [array[i], array[j]] = [array[j], array[i]];
            }
        }

        flashcardElement.querySelector('.prev').addEventListener('click', function() {
            currentIndex = (currentIndex - 1 + flashcards.length) % flashcards.length;
            showAnswer = false;
            displayCard();
        });

        flashcardElement.querySelector('.flip').addEventListener('click', function() {
            showAnswer = !showAnswer;
            displayCard();
        });

        flashcardElement.querySelector('.next').addEventListener('click', function() {
            currentIndex = (currentIndex + 1) % flashcards.length;
            showAnswer = false;
            displayCard();
        });
    });
});


