# 1 | Electrochemistry

Lecture notes are derived from multiple textbook sources, and research articles and influenced heavily by the textbooks:

## Primary Sources

1. Pauling, L. *General Chemistry*, 3<sup>rd</sup> ed.; W. H. Freeman: San Francisco, **1970**.
2. Atkins, P. W.; Jones, L. Chemical Principles: The Quest for Insight, 2<sup>nd</sup> ed.; Freeman: New York, **2002**.
3. Atkins, P. W.; Beran, J. A. *General Chemistry*, 2<sup>nd</sup> ed.; Scientific American Books : W.H. Freeman: New York, **1992**.
4. Housecroft, C. E.; Sharpe, A. G. *Inorganic Chemistry*, 4<sup>th</sup> ed.; Pearson: Harlow, **2012**.
5. Huggins, R. A. *Advanced Batteries, Materials Science Aspects*. Springer, **2008**. 
6. Schweitzer, G. K.; Pesterfield, L. L. *The Aqueous Chemistry of the Elements*; Oxford University Press; New York, **2010**.



## Lecture Slides


### Deck 1 | Oxidation states

```{image} ../assets/ECHEM/Echem-L01.png
:alt: Echem-L01.png
:width: 200px
```
Slides ([pdf](../assets/ECHEM/D1.pdf))

### Deck 2 | Redox reactions

```{image} ../assets/ECHEM/Echem-L02.png
:alt: Echem-L02.png
:width: 200px
```
Slides ([pdf](../assets/ECHEM/D2.pdf))

### Deck 3 | Electrochemical cells and their potentials

```{image} ../assets/ECHEM/Echem-L03.png
:alt: Echem-L03.png
:width: 200px
```
Slides ([pdf](../assets/ECHEM/D3.pdf))

### Deck 4 | Electrochemistry at equilibrium

```{image} ../assets/ECHEM/Echem-L04.png
:alt: Echem-L04.png
:width: 200px
```
Slides ([pdf](../assets/ECHEM/D4.pdf))

### Deck 5 | Potential, pH, and electrolysis

```{image} ../assets/ECHEM/Echem-L05.png
:alt: Echem-L05.png
:width: 200px
```
Slides ([pdf](../assets/ECHEM/D5.pdf))

### Deck 6 | Energy storage

```{image} ../assets/ECHEM/Echem-L06.png
:alt: Echem-L06.png
:width: 200px
```
Slides ([pdf](../assets/ECHEM/D6.pdf))

## Learning Objectives

1. Assign formal oxidation states for elements in chemical compounds. 
2. Determine if a chemical reaction is a redox reaction
3. Be able to express a redox reaction of a combination of reduction (electronation) and oxidation (deelectronation) half reactions. 
4. Balance redox reactions
5. Determine the number of electrons transferred in a balanced redox reaction
6. Balance a redox in aqueous acid
7. Balance a redox reaction in aqueous base
8. Be able to use the number of electrons in a redox reaction to calculate reaction yields and stoichiometries (e.g. how much time will it take to make 10 g of produce if the reaction is proceeding at 10 A, or how many grams of product will be produced by passing 20 C of charge through the cell)
9. Know and be able to convert between common electrical and electrochemical units (J, J/mol, C, V, A, Ah, mAh, W, Wh, mAh/g, Wh/g...)
10. Understand basics of electricity and electrochemical cells (cathode, anode, electrolyte, salt bridge, voltmeter, ammeter, charge, current, ...)
11. Understand the difference between energy (J or J/mol), electrochemical potential (J/C)
12. Understand the definition of an electrochemical cell
13. In an electrochemical cell that electrons and ions travel are separated. Electrons pass through the wires of a circuit and the ions pass through the electrolyte. 
14. An electrolyte is either a solution of a dissolved salt or a salt in its molten state (ionic liquid e.g. MgCl2 (l)). The electrolyte conducts electricity using only ions NOT electrons. 
15. A galvanic reaction is a spontaneous reaction.  (DeltaG < 0 and E > 0)
16. An electrolytic reaction is non-spontaneous reaction (DeltaG > 0 and E < 0)
17. A galvanic cell will produce electricity as the spontaneous reaction is allowed to proceed in forward direction by connected the two electrodes of the cell. 
18. An electrolytic cell applies an external  voltage to the cell (put energy into the reaction) to drive the reaction in the forward direction. Without the applied voltage the reaction will not favor product formation. 
19. If any cell is allowed to fully equilibrate then the measured voltage (E) will be Zero. (if the two electrodes are connected with a simple copper wire and we wait the current pass through the wire to drop to zero then the cell has equilibrated). 
20. Know how to related deltaG and electrochemical potential (E). That is: deltaG = -nFE
21. F is Faraday's constant is used to convert charge from SI units (coulombs, C) to moles electrons. F = 96485 C per mol of electron
22. Know how to identify the strongest oxidants and strongest reductants using a table of standard electrochemical reduction potentials (Eº) (aka the electrochemical series)
23. Using the electrochemical series be able to determine if a certain reductant will spontaneously reduce a certain oxidant
24. Understand the trends and how to determine the qualitative temperature dependance of electrochemical potential
25. Understand and be able to calculated non-standard electrochemical potentials (E) using the Nernst equation. 
26. Intuitively understand and apply the Nernst equation to a given redox reaction (i.e. the concentration and pH dependance of electrochemical potential)
27. Be able to use and interpret Pourbaix diagrams. 
28. Identify and classify the difference regions found on a pourbaix diagram (corrosion, cathodic protection, anodic passivation)
29. Understand the microscopic movement of ions and electrons in and electrochemical cell (both for electrolytic cells and for galvanic cells)
30. Understand and apply the concept of kinetic overpotential in electrolytic cells. 
31. Understand and apply the concept of kinetic over potential in the context of rechargeable batteries. 
32. Understand and be able to identify the difference types of electrochemical energy storage methods discussed in class (rechargeable battery, redox flow battery, fuel cell, and capacitor). 
33. Be able to interpret a Ragone plot
34. Understand the purpose and value of comparing the specific capacity, specific energy, energy density, charge density, power density, and specific power of a reaction. 
35. Be able to calculate the energy stored in battery by integrating its discharge curve. 
36. Calculate the maximum theoretical specific capacity given any galvanic reaction
37. Calculation the maximum theoretical specific energy given and galvanic reaction and its electrochemical potential. 
38. Be familiar with the different scenarios that lead to corrosion or where corrosion can be useful. 
39. Be familiar with the different implementations and basic mechanism for cathodic protection. 
40. Be familiar with the most common applications of electrolysis and electrodeposition. 
41. Be familiar with the different methods of energy storage and how to choose which one to use based on each of their advantages and limitations in different scenarios.