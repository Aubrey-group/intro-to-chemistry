# 1.10 | The Nernst Equation

**Word count:** <span id="word-count">0</span> words<br>**Reading Time:** <span id="read-time">0</span> minutes

Beyond the mechanisms and construction of electrochemical cells, fundamentally there is only one key relation that separates electrochemistry from thermochemistry and that is conversion of gibbs free energy to electrical potential: $\Delta G = -nFE$. 

So far we have used the relation to identify spontaneous and non-spontaneous reactions based upon cell voltage and the sign of $\Delta G$. We have also implemented Hess's Law to predict the cell voltage of electrochemical cells by appreciating the difference between $\Delta G$ being measured in J per mol reaction while $E$ is effectively a measure of in J per mol electrons transferred. Otherwise the fundamentals of stoichiometry and analyzing the energy balance in chemical reactions are the same.

**The Nernst Equation** likewise is merely a re-expression of the Gibbs free energy of reaction under non-standard conditions:

$$
\Delta G = \Delta G^{\circ} + RT \ln Q

$$

where $Q$ is the reaction quotient, $T$ is the temperature in Kelvin, and the ideal gas constants $R = 8.314 \frac{J}{mol\cdot K}$. Substituting $\Delta G$ for $E$ by the relation $E = \frac{-\Delta G}{n\cdot F}$ affords Nernst Equation:

$$
E = E^{\circ} - \frac{RT}{nF} \ln Q
$$

Just as with Gibbs free energy, when an electrochemical potential was not measured under standard conditions it is describe as $E$ rather than $E^{\circ}$.

## Comparison of Themochemical and Electrochemical Relationships

Below is a more complete comparison of common relations from thermochemistry that are directly applicable to electrochemistry via the relation $E = \frac{-\Delta G}{n\cdot F}$:

```{list-table}
:header-rows: 1
:widths: 15 15

*   - Thermochemistry
    - Electrochemistry
*   - $\Delta G = \Delta G^{\circ} + RT \ln Q$
    - $E = E^{\circ} - \frac{RT}{nF} \ln Q$
*   - $\Delta G = -RT \ln (K_{eq})$
    - $E = \frac{RT}{nF} \ln (K_{eq})$
*   - $\Delta G = \Delta H - T\Delta S$
    - $E = \frac{\Delta S}{nF} - \frac{\Delta H}{nF}$


```

## Cell Potential and Concentration

Show in {numref}`nerst-equation` is the dependence of the cell potential on the ratio of concentrations of the reactants and products for the half-reaction $Fe^{3+} + e^- \rightarrow Fe^{2+}$. This plot should look familiar as it is analogous to that found in acid-base chemistry for the Henderson-Hasselbalch equation. In this case, 

$$
E = E^{\circ} - \frac{RT}{nF} \ln \left( Q \right) = 
0.77\,V - \frac{RT}{nF} \ln \left( \frac{[Fe^{2+}]}{[Fe^{3+}]} \right)
$$

```{raw} html
:file: ../../assets/ECHEM/nernst.svg
```

```{figure} ../../assets/logo-dark.svg
:height: 0px
:name: nerst-equation

The dependence on the ratio of concentrations on the potential of the half-reaction $Fe^{3+} + e^- \rightarrow Fe^{2+}$. When the two concentrations are equal, $E=E^{\circ}$. As the relative concentration of $Fe^{3+}$ increases the potential becomes more positive.
```

## Cell Potential and Temperature

As with all reactions, the change in the entropy between reactants and products is the largest factor determining the temperature dependance of the cell potential. In the example shown in {numref}`cell-voltage-temperature` for the electrochemical oxidation of hydrogen gas to water, a large decrease in entropy is expected from the chemical reaction:

$$
2H_2(g) + O_2(g) \xrightarrow{\Delta V} 2H_2O(l)
$$

With three gass molecules on the left and only two liquid molecules on the right $\Delta S < 0$ for this reaction. From the relation $\Delta G = \Delta H - T\Delta S$ we can see that $\Delta G$ will become more positive as temperature increases and thus the cell voltage will *decrease* with increasing temperatures. That is, as high temperatures less useful work can be extracted from the reaction on a per electron basis. 

```{raw} html
:file: ../../assets/ECHEM/cell-voltage-temperature.svg
```

```{figure} ../../assets/logo-dark.svg
:height: 0px
:name: cell-voltage-temperature

The dependence of the cell potential on temperature for a hydrogen-oxgen fuel cell. The change in entropy of the reaction has the greatest effect on the cell potential as a function of temperature. The change in slope at 100 ºC is the result in the change in molar entropy between liquid (blue) and gaseous (red) water.
```