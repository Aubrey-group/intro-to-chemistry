# 1.5 | The Galvanic Cell


**Word count:** <span id="word-count">0</span> words<br>**Reading Time:** <span id="read-time">0</span> minutes

An electrochemical cell is a device that converts chemical energy into electrical energy by separating the pathways electron transport and ion transport. This is accomplished by physically separating the reactants by an electrolyte solution that allows ions to flow but not electrons. The reactants are then connected by a wire that allows electrons to flow from one side of the cell to the other. One example of an electrochemical reaction is shown in {numref}`cell-amps`. 

```{raw} html
:file: ../../assets/ECHEM/cell-amps.svg
```

```{figure} ../../assets/logo-dark.svg
:height: 0px
:name: cell-amps

A galvanic cell with a salt bridge and an ammeter. The anode is on the left and the cathode is on the right. Electrons flow from the anode to the cathode through the external circuit. Oxidation always occurs at the anode and reduction always occurs at the cathode. The salt bridge allows ions to flow between the two sides of the cell but prevents the mixing of the two solutions.
```

Just as for any redox reaction a net electrochemical reaction can be written for the process taking place in the cell.

$$
\begin{array}{c}

Zn(s)\;\; && + && \;\;Cu^{2+}(aq)\;\; && +\;\;&& SO_{4}^{2-} (aq)\;\; && \xrightarrow{\;\;\;\;\;\;} \;\; && Zn^{2+}(aq) \;\; &&+&& Cu(s) \;\; && +\;\;  && SO_{4}^{2-} (aq) \\
\textcolor{#E57575}{\text{Anode}} && && && && && && && && \textcolor{#8CAEE7}{\text{Cathode}}&& &&  \\

\end{array}
$$

A **Galvanic cell** is a type of electrochemical cell that generates electrical energy from a spontaneous redox reaction. The reaction between $Zn$ and $Cu^{2+}$ is a spontaneous reaction, $\Delta G < 0\,kJ/mol$. A spontaneous electrochemical reaction is called a **Galvanic reaction**. When the Zn and Cu are connected by a wire, electrons flow from the Zn to the Cu (current flows from Cu to Zn). This spontaneous flow of electrons can then be used to do work.

## Components of a Galvanic Cell

A galvanic cell consists of the following components:

1. **Electrode**: A solid electron conductor, usually a metal, that is in contact with the electrolyte solution. In the cell shown in {numref}`cell-amps` the electrodes are Zn and Cu.
2. **Anode**: The electrode where oxidation takes place. In the cell shown in {numref}`cell-amps` the anode is the Zn electrode.
3. **Cathode**: The electrode where reduction takes place. In the cell shown in {numref}`cell-amps` the cathode is the Cu electrode.
4. **Electrolyte**: A molten salt or solution of dissolved ions that conduct electricity by the movement of ions. Electrolytes do not conduct electrons. In the cell shown in {numref}`cell-amps` the electrolyte is an aqueous solution of zinc sulfate at the anode and copper sulfate at the cathode. The two solutions are in contact with each other in the middle of the cell via a porous glass frit.
5. **Catholyte**: The electrolyte at the cathode. In the cell shown in {numref}`cell-amps` the catholyte is the copper sulfate solution.
6. **Anolyte**: The electrolyte at the anode. In the cell shown in {numref}`cell-amps` the anolyte is the zinc sulfate solution.
7. **Salt Bridge**: A porous barrier that allows ions to conduct between the two electrolytes but prevents the mixing of the two solutions. A common example of a salt bridge is a gel filled with an ancillary electrolyte salt like sodium sulfate. In the cell shown in {numref}`cell-amps` a porous glass frit is used instead of a salt bridge that allows the flow of sulfate ions between the two solutions.
8. **Glass Frit**: A porous glass disk that allows two solutions to be in contact while preventing the rapid mixing of the two solutions.
9. **Circuit**: A network of electron conductors (like copper wire) converts the flow of electrons into useful work. In the cell shown in {numref}`cell-amps` the circuit is completed by the ammeter.
10. **Ammeter**: A device that measures the rate of flow of electrons in the circuit. 

After some time of allowing the reaction to proceed spontaneously the Zn electrode will be consumed as it is oxidized into $Zn^{2+}\,(aq)$ and the Cu electrode will be plated with additional Cu (s) as $Cu^{2+}\,(aq)$ is reduced. {numref}`cell-amps-finished` shows the cell after some time of operation.

```{raw} html
:file: ../../assets/ECHEM/cell-amps-finished.svg
```

```{figure} ../../assets/logo-dark.svg
:height: 0px
:name: cell-amps-finished

The galvanic Zinc-Copper cell after some time of operation. The Zn electrode is consumed as it is oxidized into $Zn^{2+}\,(aq)$ and the Cu electrode is plated with additional Cu (s) as $Cu^{2+}\,(aq)$ is reduced. The concentration of zinc ions in solution has increased and the concentration of copper ions has decreased.
```

We'll see later several examples of electrochemical reactions that are not spontaneous ($\Delta G > 0$) and require an external source of electricity to proceed in the forward direction. These are called **Electrolytic reactions**. One example would be if we wanted to reduce $Zn^{2+}$ to $Zn$ in solution and oxidize $Cu$ to $Cu^{2+}$ in solution; that is run the cell in {numref}`cell-amps-finished` in reverse.

## Half-Reactions and Half-Cells

As we've seen an electrochemical cell divides a redox reaction into two halves. In {numref}`cell-amps` the oxidation half occurs on the left side converting $Zn\,(s)$ into $Zn^{2+}$ and sends two electrons into the circuit. For this half of the cell we can express this process as a **half-reaction**. A half-reaction is a hypothetical reaction of *pure* reduction (aka electronation) or *pure* oxidation (aka deelectronation). In the case of the Zn electrode the half-reaction is: $Zn\,(s) \rightarrow Zn^{2+} + 2e^-$. Likewise reduction half-reaction at the Cu electrode is: $Cu^{2+} + 2e^- \rightarrow Cu\,(s)$. Just as we used the idea of half-reactions to balance redox reactions we can spatially separate these halves for the purposes of generating electrical energy from a chemical reaction. The physical half of the cell where a half-reaction occurs is all a **half-cell**. 

## Shorthand Notation for Electrochemical Cells

Rather than drawing out the entire construction of an electrochemical cell it becomes convenient to have a shorthand that mimics the construction of the cell. The shorthand notation for the cell in {numref}`cell-amps` would be: 

$$
| \;Zn\; |\; 1\; M\; ZnSO_4\; (aq)\; ||\; 1M\; CuSO_4\; (aq)\; |\; Cu\; |
$$

The anode is always on the left and the remainder of the cell is written as we proceed from left to right across the physical cell. If the concentrations and solvent are not provided it is assumed that the solutions are 1 M and the solvent is water. The vertical lines in the cell notation denote *phase boundaries*. The left and right most vertical lines denote the connection point of the electrodes to the external circuit (not shown in the notation). A salf bridge or other electrolyte barrier is denoted by a double vertical line. 