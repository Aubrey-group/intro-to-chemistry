# Appendix 2 | Glossary

```{glossary}

Electrochemistry
    The interaction of molten salts or ionic solutions with electric currents

Voltage
    How hard electrons are ‘pushing’ to move between two locations. The amount of energy (J) gained or lost per electron transferred between two locations. Units: Volt = V = J/C

Current
    How many or how fast electrons are flowing. Units: Amp=A=C/s

Charge
    A fundamental property of subatomic particles as in p+ and e–. The origin of electricity. Units: C=coulomb

Cell Potential
    A voltage measured between chemically distinct locations at zero current. Units: Volt = V.


Electrolysis
    A chemical reaction induced by applying a voltage.

Oxidation
    A net loss of electrons relating a reactant to a product.

Reduction
    A net gain of electrons relating a reactant to a product.

Half Reaction
    A chemical reaction that explicitly shows a gain (reduction) or loss (oxidation) of electrons in the reaction scheme.

Electrochemical Cell
    A device that forces electrons through one path (usually an external circuit) and ions through another (the electrolyte)

Cathode
    The electrode in an electrochemical cell where reduction occurs.

Anode
    The electrode in an electrochemical cell where oxidation occurs.

Electrolyte
    The medium (usually a liquid) in an electrochemical cell that conducts ions but not electrons

Salt Bridge
    A specialized device that allows a common ion to conduct between two electrolyte solutions while minimizing the mixing of all other ions between two half cells.

Faraday
    The amount of charge in 1 mol of electrons. 1 F = 96,485 C  

Metal
    Material in which electrons flow freely. Conductivity increases a temperature decreases

Semiconductor
    Material the conducts electrons but not freely. Need heat to conduct electrons (themal excitation). Conductivity increases a temperature increases.

Ohm
    SI unit of resistance. Units of R in Ohms law relating current observed to voltage applied in Ohm's Law: $V=IR$

Siemen
    SI unit of conductivity equal to 1/Ohm

Resistivity
    An intrinsic property of a material that describes how hard it is for electrons to flow through a material of a given chemical composition and structure. The resistivity of a material is normalized to the length and cross-sectional area of the sample through which current passes: $\rho = \frac{R\times Area}{length} Units: Ohm-cm

Conductivity
    A measure of how easy it is for charge (ionic or electronic) to flow through a material or solution. $\sigma = 1/\rho$ Units: S/cm

Contact Ion Pair
    A positive and a negative charged pair of ions that are in solution but electrostatically bound to each other and "appear" to be effectively charge neutral in the presenece of an electric field. Contact ion pairs do not conduct electricity.

Electrical Double Layer
    The layer of ions right at an electrode surface under applied voltage that forms multiple layers of ions of opposite charges.

Dendrite
    Tree-like metallic structures that sometimes form when electrodepositing a metal in an electrochemical cell
```