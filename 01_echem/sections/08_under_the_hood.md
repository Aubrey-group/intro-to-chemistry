# 1.8 | Atomistic View of Electrochemical Cells

**Word count:** <span id="word-count">0</span> words<br>**Reading Time:** <span id="read-time">0</span> minutes

On the macroscopic scale, electrons in an electrochemical cell flow from the anode as it is oxidized through the external circuit to the cathode where the electrons are consumed in the reduction half reaction. All the while, in the electrolyte solution if a Zn<sup>2+</sup> cation is produced on the anode side of the cell and a Cu<sup>2+</sup> is consumed on the cathode side of the cell then there is one too few sulfate ions on the left half of the cell to charge balance Zn<sup>2+</sup> and there are one too many sulfate ions on the right half of the cell. Thus for charge balance a net transport of 1 sulfate from the cathode to the anode must take place via ion conduction through the electrolyte. If the reaction is spontaneous (as in a galvanic cell) the flow of electrons can be used to do work in an external circuit (like a flashlight or a cell phone).


```{raw} html
:file: ../../assets/ECHEM/cell-charge-transfer.svg
```

```{figure} ../../assets/logo-dark.svg
:height: 0px
:name: cell-charge-transfer

The net transfer of charge as electrons and ios within the galvanic zinc-copper cell with a zinc sulfate electrolyte at the anode and a copper sulfate electrolyte at the cathode. Electrons flow from the anode to the cathode through the external circuit. Negatively charged, sulfate ions (SO<sub>4</sub><sup>2–</sup>) flow from the cathode to the anode through the electrolyte. 
```

## Electron Conductors: Metals and Semiconductors

All electrodes and the materials that connect two electrodes together to complete the external circuit *must* conduct electrons. Electronic conductivities in materials can take on a very large range of values from 10<sup>-19</sup> S/cm in the best electronic insulators up to 10<sup>5</sup> S/cm in the most conductive metals.

Unlike molelcules which we can envision the bonding of as interactions of only two to a few dozen bonding and antibonding orbitals, in solids there are an enormous number of atoms such that hundreds of thousands of atomic orbitals overlap to form continuous bands of energy levels. Instead of drawing all these lines in out molecular orbital diagrams instead a block or *band* of continuous energy levels is drawn, {numref}`metals-and-semiconductors`. As with molecules these bands can either be filled, empty, or partially filled.

Bands that are completely filled are called *valence bands*. Electrons in valence bands are not conductive. Bands that are empty or partially filled are called *conduction bands*. If there are some in conduction bands they are free to move through the material. Metals have partially filled conduction bands and are good conductors of electricity. Semiconductors have empty conduction bands since there are no electrons in the conduction band they are not conductive. However, if enough energy is added to the material (*e.g.* as heat or light) electrons can be excited to the conduction band and the material can become conductive. if the gap between the valence and conduction bands is small (<1 eV) the material is called a *semiconductor*. If the gap is large (> 1 eV) the material is called an *insulator*.



### Metals

In metals, some of the valence electrons can move freely through the material largely unbound to any metal atom in particular. That is, electrons are itinerant. To a first approximation the state of these itinerant valence electrons can be considered to act as a gas of electrons that are free to move and diffuse through the solid, {numref}`metals-and-semiconductors`. This is why metals are good conductors of electricity. The flow of electrons is only impeded by scattering events where electrons are deflected by the vibrations of the metal atoms, defects, or impurities in the metal. Because of this, metals tend to have very high electronic conductivities usually on the order of 100 to 100,000 S/cm. **As temperature increases** metal atoms vibrate more scattering more electrons and **conductivity decreases**. 

### Semiconductors

While we have so far focused solely on electrodes made of metal, semiconductors can also be used as electrodes. For example some battery materials, such as LiFePO<sub>4</sub>, are semiconducting. Electrons do not flow freely in semiconductors. Instead the electrons are localized to specific atoms or groups of atoms in the solid. However, if enough heat is added to the material electrons can be excited to either hop between different sites or excited to a higher energy state where they can move freely through the material akin to metallic electrons. 

Regardless of the exact mechanism (hopping vs excitation into a conductive state) **as the temperature of a semiconductor increases the conductivity of the material increases**. Typical conductivities of semiconductors range from ~1 S/cm in doped Si to as low as 10<sup>-9</sup> S/cm in LiFePO<sub>4</sub>. Materials with conductivities below 10<sup>-9</sup> S/cm are usually considered good insulators.



```{raw} html
:file: ../../assets/ECHEM/metal-semiconductor.svg
```

```{figure} ../../assets/logo-dark.svg
:height: 0px
:name: metals-and-semiconductors

In solids many orbitals overlap to form continuous bands of crystal orbitals. In metals the the conduction band is partially filled. In semiconductors the valence band is filled and the conduction band is empty, but heat or light can excited electrons into the conduction band (left). Metals have itinerant electrons whose conductivity is limited only by scattering events (right).
```

## Ion Conductors

Unlike electrons ionic charge carriers like a Cu<sup>2+</sup> cations or a SO<sub>4</sub><sup>2-</sup> anion are much heavier and move much more slowly. Whether in solution, a molten salt or in the solid state we can imagine the motion of ions to occurs as as series of "hop" from one stable position to another. {numref}`ion-hopping` shows a schematic of how this may occur in a crystal at very high temperatures when a vacancy defect is present that allows the ions to hop from one site to another. Within molten salts and dissolved ions in solution the structure of the melt or solution of much less ordered we can apply a similar model to accurate describe ion transport in these systems as well. 


```{raw} html
:file: ../../assets/ECHEM/ion-hopping.svg
```

```{figure} ../../assets/logo-dark.svg
:height: 0px
:name: ion-hopping

Ion hopping between vacancy defects (missing cations) in an ionic crystal a very high temperatures (left). The thermal barrier to ion hopping plotted as a function of the distance the cation travels through the crystal. 
```

### Molten Salts

In molten salts ions are free to diffuse past one another in the liquid state. However there is an energy barrier for doing so. The more kinetic energy an ion has the more likely it is to overcome the barrier to transport ({numef}`ion-hopping`) and the higher the conductivity of ions in the salt melt will be. Thus ionic conductivity in molten salts always increases with temperature and is generally higher for smaller ions than for larger ions under the same conditions. Given the very high concentration of ions in a salt melts the conductivities are high for ion conductor (around 0.01 to 10 S/cm).

### Electrolyte Solutions

In electrolyte solutions ions are dissolved in a solvent. Many factors an impact the conductivity of a solution. Metal cations are often coordinated by solvent molecules and anions are will have some degree of ordered solvent molecules surrounding the anions as well. While the ion can diffuse through solution this requires significant rearrangement of the solvent along with the movement of the ion. Because of this there is a thermal barrier to ion transport through solution akin to the scenarios found at high temperatures in crystals and molten salts. The viscosity of a solution can also limit the conductivity of a solution. The higher the viscosity the lower the ionic conductivity. The same is true for molten salts which experience a dramatic drop in conductivity as they cool and solidify.


Unlike molten salts the *concentration* of ions can be controlled by dissolving more of less of a salt in the solvent. At low concentrations ionic conductivity increases with increasing concentration. However, at high concentrations in solution (~1 M) ions can pair with other ions of opposite charge to form ion pairs that are effectively charge neutral and do not contribute to the conductivity of the solution. At very high concentrations while the ions are still dissolved in solution the conductivity of the solution will decrease with increasing concentration, {numref}`conductivity-vs-concentration`. This difference in structure between having dissociated ions in solution and ion pairs dissolved in solution can be described as an equilibrium:



$$
\begin{array}{cccccc}
NaCl\,(s)   & \eqarrow{K_{sp}} & Na^+\,(aq) + Cl^-\,(aq) & \eqarrow{} & [Na^+][Cl^-]\,(aq) \\
& & \text{dissociated ions} & &  \text{ion pair}
\end{array}
$$


```{raw} html
:file: ../../assets/ECHEM/ions-in-solution.svg
```

```{figure} ../../assets/logo-dark.svg
:height: 0px
:name: conductivity-vs-concentration

Ion hopping between vacancy defects (missing cations) in an ionic crystal a very high temperatures (left). The thermal barrier to ion hopping plotted as a function of the distance the cation travels through the crystal. 
```

## Electrode Polarization and the Electrical Double Layer

With a base knowledge of how ions and electrodes conduct through an electrochemical cell we can now reconstruct the mechanism of an electrochemical reactions at the atomic scale, {numref}`electrical-double-layer`.

```{raw} html
:file: ../../assets/ECHEM/electrical-double-layer.svg
```

```{figure} ../../assets/logo-dark.svg
:height: 0px
:name: electrical-double-layer

The electrical double layer that forms at the surface of an electrode in an electrochemical cell. 
```

1.  When a voltage is either applied between two electrodes *or* two metal electrodes like Zn and Cu *produce* a voltage in proportion to their free energy of reaction charges in the cell will **polarize**.
2. Electrons move much faster than ions. The first thing to happen is electrons will flow through the metallic circuit between the electrodes. Positive charge (depletion of electrons) will build up on the anode and negative charge (excess of electrons) will build up on the cathode. This is called **electrode polarization**.
3. Electrode polarization will produce and electric field across the electrolyte. 
4. Ions in solution will move in response to the electric field. Cations will move toward the cathode and anions will move toward the anode.
5. The build up of ions at the electrode surface will result in a high concentration of anions at the anode and cations at the cathode. In turn, a second layer of cations will form around the anode and a second layer of anions will form around the cathode. This is called the **electrical double layer** which screens the bulk of the electrolyte from the electric field generated by the polarized electrodes.
6. In the case of the zinc-copper cell the large build up of Cu<sup>2+</sup> (*aq*) at the cathode surface is favorable for the reduction of Cu<sup>2+</sup> to Cu<sup>0</sup> (*s*) and the large build up SO<sub>4</sub><sup>2–</sup> at the anode surface is favorable for the oxidation of Zn<sup>0</sup> (*s*) to Zn<sup>2+</sup> (*aq*).
