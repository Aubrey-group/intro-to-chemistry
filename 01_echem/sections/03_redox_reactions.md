# 1.3 | Redox Reactions

**Word count:** <span id="word-count">0</span> words<br>**Reading Time:** <span id="read-time">0</span> minutes

Chemical reactions in which a species changes oxidation state are called redox reactions. An **oxidation** can be identified by an increase in a specie's oxidation state or formal loss of electrons as in $Fe^{2+} \rightarrow Fe^{3+} + e^-$. A **reduction** can be identified by a decrease in a specie's oxidation state or formal addition of electrons as in $Cu^{2+} + 2e^- \rightarrow Cu^{1+}$. Generally for any real balanced reaction one reactant cannot be oxidized without another being reduced in order to maintain charge balance. We refer to these as "**redox reactions**".

## Half-reactions describe only either pure oxidation or pure reduction

In one useful case however we can write *half-reactions* that are reactions of pure reduction (aka electronation) and pure oxidation (aka deelectronation). For example, the half reaction $Fe +  \rightarrow Fe^{2+} + 2 e^-$ is a reduction half-reaction. We call these half reactions because that are typically not possible to observe in isolation. Within a vacuum in the presence of ionizing radiation perhaps the ionization of iron could be observed, but it is generally not possible in aqueous solution where as we will see the definition of a half-reaction can be used to describe a great many reactions commonly observed in the laboratory.

## Identifying redox reactions

When given or proposing a reaction you should get into the habit of always determining oxidation states first. 

1. If an element's oxidation state increases going from reactants to products, it is oxidized.
2. If an element's oxidation state decreases going from reactants to products, it is reduced.
3. It is possible for no oxidation state to change (as in a simple acid-base reaction)
4. It is also possible for other types of reactions (acid-base, dissolution, ...) and redox reactions to take place within the same reaction.

## Practice: Identifying oxidants and reductants in chemical reactions

For each of the following, identify the species that is oxidized and the species that is reduced.

<div id="flashcard2" class="flashcard" data-file="../../_static/redox-id.json" 
    style="display:grid; grid-template-rows: 1fr auto; min-height:200px; margin:auto">
    <div id="content" class="content" style="display:flex;margin:auto;"></div>

<div style="display:flex;justify-content:center; align-items:center;">
<button id="prev" class="prev">Previous</button>
<button id="flip" class="flip">Flip</button>
<button id="next" class="next">Next</button>
</div>
</div>

## Types of redox reactions

In addition to the generic redox reaction that classifies all chemical reactions that involve a change in oxidation state, there are several specific types of redox reactions that are commonly encountered in chemistry. These include:

1. **Comproportionation**: A redox reaction in which a species is in two different oxidations states in the reactants and is converted to a single oxidation state in the products. One example is the comproportionation of CuCl<sub>2</sub> and Cu metal to form CuCl: $CuCl_2 + Cu \rightarrow 2CuCl$.
2. **Disproportionation**: A redox reaction in which a species is in a single oxidation state in the reactants and is converted to two different oxidation states in the products. One example is the disproportionation of mercury in the reaction $Hg_2Cl_2 + heat \rightarrow HgCl_2 + Hg (l)$.
3. **Combustion**: A redox reaction in which a fuel (usually an organic compound) is oxidized (usually with oxygen gas) to produce carbon dioxide, water, and heat. One example is the combustion of methane: $CH_4 + 2O_2 \rightarrow CO_2 + 2H_2O$. Note the change is oxidation state of carbon from -4 to +4 and oxygen from 0 to -2.


