# 1.4 | Balancing Redox Reactions

**Word count:** <span id="word-count">0</span> words<br>**Reading time:** <span id="read-time">0</span> minutes

To balance a redox reaction not only must the number of atoms of each element be the same on both sides of the equation but also the number of electrons. In simple cases this is born out in the charges. For example consider the reaction between iron and tin: $Fe^{2+} + Sn^{4+} \rightarrow Fe^{3+} + Sn^{2+}$. This reaction is not balanced because the number of electrons (and charges) on the left is not equal to the right. While perhaps this case is simple enough to balance by inspection the systematic process of decomposing a redox reaction into two half-reactions is more reliable especially for more complex net reactions.

## Balancing Redox Reactions using Half-Reactions

**First**: To separate a redox reaction identify which species is oxidize and which is reduced and separate them by writing two half-reactions.

$$
\begin{align*}
\text{(reduction)} &\quad Fe^{2+} \rightarrow Fe^{3+} + e^- \\
\text{(oxidation)} &\quad Sn^{4+} + 2e^- \rightarrow Sn^{2+}
\end{align*}
$$

**Second**: The determine the least common multiple of the electron stoichiometries of the half-reactions and multiply each half-reaction by the appropriate factor. 

 

$$
\require{cancel}
\begin{align*}
  &\quad 2Fe^{2+} \rightarrow 2Fe^{3+} + \cancel{\textcolor{darkorange}{2e^-}} &\quad (\times 2)\\
  +  &\quad Sn^{4+} + \cancel{\textcolor{darkorange}{2e^-}} \rightarrow Sn^{2+} &\quad (\times 1) \\
\hline
    &\quad 2Fe^{2+} + Sn^{4+} \rightarrow 2Fe^{3+} + Sn^{2+} &\quad \text{(net redox)}
\end{align*}
$$

**Finally**: Add the two half-reactions together and cancel out any common terms.

Above the first reaction is multiplied by 2 and the second by 1 not that the the pair of a reduction and oxidation half-reaction should always result in a complete cancellation of the electrons. *Electrons should never appear in the balanced net reaction*.

### Practice

For each of the following balance the redox reaction:

<div id="flashcard2" class="flashcard" data-file="../../_static/redox-simple.json" style="display:grid; grid-template-rows: 1fr auto; min-height:400px; margin:auto">
    <div id="content" class="content" style="display:flex;margin:auto;"></div>

<div style="display:flex;justify-content:center; align-items:center;">
<button id="prev" class="prev">Previous</button>
<button id="flip" class="flip">Flip</button>
<button id="next" class="next">Next</button>
</div>
</div>

## Balancing Redox Reactions in Aqueous Solutions

In aqueous solution, while $H_2O$, $H^+$, and $OH^-$ may not play a central role in the redox reaction they are every present durring the reaction. Many redox reactions are in fact both an acid-base reaction and a redox reaction occurring simultaneously where the inclusion of $H_2O$, $H^+$, and $OH^-$ is necessary balance the overall net reaction. For example consider the redox reaction between PbO$_2$ and $NaI$ in aqueous acid. Assuming we know what we put into solution and that the presences $Pb^{2+}$, $I_2$ can be experimentally determined spectroscopically we may naively hypothesize a redox reaction. 


$$
\begin{align*}
PbO_2 (s) + NaI (aq) &\xrightarrow{H_2O,\,acid} Pb^{2+} (aq) + I_2 (aq) + 2Na^{+}
\end{align*}
$$



### Acidic Aqueous Solutions


To balance a reaction in aqueous acidic solutions we just have to potentially account for acid-base reactions with the solvent participating:

1. Separate the redox reaction into half-reactions by identifying the main species that are oxidized and reduced.
2. Balance the main atom in the half-reaction
3. Add the appropriate number of electrons to account for the change in oxidation state
4. Add water to balance the oxygen atoms
5. Add $H^+$ as needed to balance the hydrogen atoms
6. Determine the least common multiple of electrons for the two half-reactions
7. Multiply the half-reactions by the appropriate factor to balance the electrons
8. Add the half-reactions together and cancel out any common terms. Electrons to cancel completely. Electrons should never appear in the balanced reaction.

For the example above Pb is reduced from +4 to +2 and I is oxidized from -1 to 0. Separating species into appropriate half-reactions:

$$
\begin{align*}
PbO_2 + 2e^- \rightarrow Pb^{2+}  &\quad \text{(reduction)}\\
2NaI \rightarrow I_2 + 2e^- + 2 Na^+ &\quad \text{(oxidation)}
\end{align*}
$$

The oxidation of $NaI$ is charge adn stoichiometrically balanced after added the electrons. No need to add water or $H^+$. However after balancing the main atom and adding electrons to account for the change in oxidation state the lead half reaction is still not balanced since $O$ appears on the left but not the right. Add two $H_2O$ molecules to the right to balance the oxygen atoms then $H^+$ to the left to balance the hydrogen atoms.

$$
\begin{align*}
PbO_2 + 4 H^+ + 2e^- \rightarrow Pb^{2+} + 2 H_2O &\quad \text{(reduction)}\\
2NaI \rightarrow I_2 + 2e^- + 2 Na^+ &\quad \text{(oxidation)}
\end{align*}
$$

Now the lead half reaction is mass balanced and summing the charges for each side we see that both the left and the right side sum to 2+. The lead reaction is now mass, charge, and electron balanced. Finally multiply both the top and bottom reactions by 1 and add them together to get the net redox reaction.



$$
\begin{alignat*}{2}
&\quad \begin{array}{lrl}
     & PbO_2 + 4 H^+  +2e^- & \rightarrow Pb^{2+} + I_2 + 2 H_2O &\quad (\times 1) \\
    + & 2NaI                & \rightarrow I_2 + 2 e^- + 2 Na^+   &\quad (\times 1) \\
  \end{array} \\
  \hline
& \quad\quad\quad PbO_2 + 4 H^+ + 2 NaI  \rightarrow Pb^{2+} + I_2 + 2 Na^+ + 2 H_2O 
\end{alignat*}
$$



```{admonition} Aqueous acidic solutions
Never invoke $OH^-$ if a reaction occurs in acidic solutions. Use $H^+$ and $H_2O$ to balance the reaction.
```

#### Practice

For each of the following balance the redox reaction in aqueous acidic solutions:
<div id="flashcard3" class="flashcard" data-file="../../_static/redox-acid.json" style="display:grid; grid-template-rows: 1fr auto; min-height:400px; margin:auto">
    <div id="content" class="content" style="display:flex;margin:auto;"></div>

<div style="display:flex;justify-content:center; align-items:center;">
<button id="prev" class="prev">Previous</button>
<button id="flip" class="flip">Flip</button>
<button id="next" class="next">Next</button>
</div>
</div>

### Basic Aqueous Solutions

We could also balance the same reaction in basic solutions. The process is similar with just two additional steps:

1. Separate the redox reaction into half-reactions by identifying the main species that are oxidized and reduced.
2. Balance the main atom in the half-reaction
3. Add the appropriate number of electrons to account for the change in oxidation state
4. Add water to balance the oxygen atoms
5. Add $H^+$ as needed to balance the hydrogen atoms
6. **Add $OH^-$ to both sides of the half-reaction to neutralize $H^+$**
7.  **Combine any $H^+$ and $OH^-$ to form $H_2O$.** $H^+$ should never appear in a balanced reaction under basic conditions.
8. Determine the least common multiple of electrons for the two half-reactions
9. Multiply the half-reactions by the appropriate factor to balance the electrons
10. Add the half-reactions together and cancel out any common terms. Electrons to cancel completely. Electrons should never appear in the balanced reaction.

After adding $OH^-$ to both sides (steps 1–6) of the lead half-reaction we have:

$$
\begin{align*}
PbO_2 + 4 H^+ + 4 OH^- + 2e^- &\rightarrow Pb^{2+} + 2 H_2O + 4 OH^- \\
2NaI &\rightarrow I_2 + 2e^- + 2 Na^+ &\quad 
\end{align*}
$$

Neutralizing the $H^+$ and $OH^-$ (step 7) we get:

$$
\begin{align*}
PbO_2 + 4 H_2O + 2e^- &\rightarrow Pb^{2+} + 2 H_2O + 4 OH^- \\
2NaI &\rightarrow I_2 + 2e^- + 2 Na^+  
\end{align*}
$$


From here the process is the same as for acid (steps 8–10). Adding the two half-reactions together and canceling out common terms we get:

$$
\begin{alignat*}{2}
&\quad \begin{array}{lrl}
     & PbO_2 + 2\,\cancel{4} H_2O + \cancel{2e^-} & \rightarrow Pb^{2+} + \cancel{2 H_2O} + 4 OH^- &\quad (\times 1) \\
    + & 2NaI                & \rightarrow I_2 + \cancel{2 e^-} + 2 Na^+   &\quad (\times 1) \\
  \end{array} \\
  \hline
& \quad\quad\quad PbO_2 + 2 H_2O + 2 NaI  \rightarrow Pb^{2+} + I_2 + 2 Na^+ + 4
\end{alignat*}
$$

#### Practice

For each of the following balance the redox reaction in aqueous acidic solutions:
<div id="flashcard4" class="flashcard" data-file="../../_static/redox-base.json" style="display:grid; grid-template-rows: 1fr auto; min-height:400px; margin:auto">
    <div id="content" class="content" style="display:flex;margin:auto;"></div>

<div style="display:flex;justify-content:center; align-items:center;">
<button id="prev" class="prev">Previous</button>
<button id="flip" class="flip">Flip</button>
<button id="next" class="next">Next</button>
</div>
</div>
