# 1.6 | Electricity in Electrochemistry

**Word count:** <span id="word-count">0</span> words<br>**Reading Time:** <span id="read-time">0</span> minutes

In electrochemistry, electricity originates from the flow (i.e. current) and build up (i.e. a concentration gradient) of charge. There are two types of charge carriers: electrons and ions. 

Charge flows through wires as electrons. The movement of charge through a wire can be though of as either a flow of electron (negative charge) in one direction or as a flow of positive charge in the opposite direction. The standard measurement of this current is the **Ampere (A)** which, perhaps counterintuitively, is a measure of the flow of positive charge. In metals, it is always the electrons that are moving through the circuit and not the positive charge balancing atoms that compose the mass and structure of the material. Metals generally cannot conduct electricity via the movement of ions.

Electricity can also be conducted via the movement of ions. Ions conduct electricity either within molten salts, such as liquid NaCl at 800 °C, or in a solution of dissolve salt, such as a 1 M aqueous solution of NaCl. Typically solutions of dissolved salts do not conduct electricity via electrons. Because ions are much larger and heavier than electrons, the mobility of ions in the most conductive electrolyte solutions is much lower than that of electrons in metals. 


## Units of Charge and Current

Using electrochemical cells we now have a new measure of the *extent* or a chemical reaction. Instead of determining masses, concentrations, or partial pressures, the measure of *current* can be used to directly determine the number of electrons that have been transferred in the redox reaction. 

In electrochemistry we will use a variety of electrical units to analyze chemical systems. We may write a half reaction as in:

$$
Fe^{2+} + 2e^- \rightarrow Fe
$$

that can be describe as one mole of $Fe^{2+}$ being reduced to one mole of $Fe$ by the transfer of two moles of electrons. Faraday's constant, $F$, connects the number of moles of electrons to the SI electrical unit of charge, the **Coulomb**, where $1\,F = 96,485\,C/mol$. The charge of 1 mole of electrons is $96,485\,C$ and the charge of 1 electron is $1.602 \times 10^{-19}\,C$.

In practice we do not often count Coulombs directly but instead measure the rate electrons flow in a circuit called the **current**. Current is measured in units of **Amperes (or Amps)** (A) where $1\,A = 1\,C/s$. A current of 1 Amp is the flow of 1 Coulomb of charge per second. An **ammeter** is a device that measures current.

A common unit of current in engineering and consumer electronics is the **Amp-hour (Ah)** or milliamp-hour (mAh) where $1\,Ah = 3600\,C$ and $1\,mAh = 3.6\,C$. By dimensional analysis we see that if $1\,A = 1\,C/s$ then $1\,Ah = 1\,C/s \times 3600\,s = 3600\,C$. since there are 3600 seconds in one hour. Take a look at any rechargeable battery or the specifications for your laptop of cell phone and you'll see the number of electrons that it can deliver to a circuit measured in Ah or mAh.


## Voltage and Gibb's Free Energy

**Voltage** is a measure of potential energy per unit charge and has units of **Volts (V)** where $1\,V = 1\,J/C$. By Faraday's constant we can convert volts to units more familiar to chemistry where $1\,V = 96485 \frac{J}{mol\,e^-}$. Voltage can be measured using a **voltmeter**. We can think of voltage as a driving potential that pushes electrons to move from one side of a cell (or circuit) to the other. Voltage (J/C) is *not* equivalent to energy (J).

In thermochemistry the capacity for a reaction to do work is measured as a change in the Gibb's free energy, $\Delta G$. The energy balance of a redox reaction can be related to the voltage of the cell by the equation: $\Delta G = -nFE$ where $n$ is the number of moles of electrons transferred in the reaction, $F$ is Faraday's constant and $E$ is the voltage of the cell when zero current is passing through the circuit. This equation is a merely a unit conversion by first converting C to mol e<sup>-</sup> then converting mol e<sup>-</sup> to mole equivalents of the reaction, then finally multiplying by minus 1 (convention). For example:

$$
Fe^{2+} + 2e^- \rightarrow Fe \quad E = 0.77\,V 
$$

converting to a Gibb's free energy:

$$
\Delta G_{rxn}=-1\times \left( \frac{0.77 J}{C} \right)\left( \frac{96485 C}{mol\,e^-} \right)\left( \frac{2\,mol\,e^-}{1\,mol_{rxn}} \right) = -150,000\,\frac{J}{mol_{rxn}}
$$

```{admonition} Electrons transferred per mol reaction ($n$)

Whenever balancing redox reactions take careful note of the final number of electrons transferred in the balanced reaction. This number is used to convert between moles of electrons and moles of the reaction. For a half-reaction as in the example above $n = 2$ and clearly identifiable in the balanced half-reaction. Likewise for the net redox reaction 

$$
3 Na (s) + Fe^{3+} \rightarrow 3 Na^{+} + Fe (s)
$$

the number of electrons transferred is $n = 3$. This is for every 3 moles of sodium that are oxidized 3 moles of electrons are transferred. For every 1 mole of Fe<sup>3+</sup> that is reduced 3 moles of electrons are transferred. While the electrons do not appear in the net reaction the mole ratio of electrons transfered is still observed in an electrochemical cell by the amount of current that flows through the circuit, the amount of work that can be done by the cell. By measuring the current over time the amount of iron that is deposited can be determined if $n$ is known.

```

## Ohm's Law

For a conductor like a metal or a an electrolyte solution the current resulting from the flow of charge is proportional to the voltage applied across the conductor. This relationship is described by **Ohm's Law**: $ V = IR$ where $V$ is the voltage, $I$ is the current, and $R$ is the resistance of the conductor. The resistance of a conductor is measured in units of **Ohms (Ω)** where $1\,\Omega = 1\,V/A$. Resistance is a measure of how much the conductor resists the flow of charge. 

The inverse of resistance is conductance, $G = \frac{1}{R}$, where the conductance, $G$, has units of **Siemens (S)** where $S=\Omega ^{-1}$. Conductance is a measure of how well the a conductor or electrolyte solution conducts electricity.

When comparing materials (*e.g.* different metals) or different electrolyte solutions (*e.g.* 1 M NaCl vs 1 M NaSO<sub>4</sub>) we care not about the physical size and shape of the sample but rather how the intrinsic properties of say NaCl and  NaSO<sub>4</sub> that lead to differences in their resistance to the flow of charge. We can eliminate the extrinsic factors of size and shape by normalizing the resistance to both length and the cross-sectional area of the conductor to get the **resistivity** of the material, {numref}`conductivity-expt`. We may re-express Ohm's law as $E = \rho j$. Here, $E$ is the electric field in (V/cm) of the applied voltages divided by the distance between the electrodes. The current density, $j$ has units A/cm<sup>2</sup>, and is the current divided by the cross-sectional area of the conductor. The resistivity, $\rho$, has units of Ohm cm and is a constant for each conductors unique chemical composition and structure.  

The multiplicative inverse of resistivity is conductivity, $\sigma$, which has units of S/cm. Conductivity is a measure of how well a material conducts electricity.

```{raw} html
:file: ../../assets/ECHEM/conductivity.svg
```

```{figure} ../../assets/logo-dark.svg
:height: 0px
:name: conductivity-expt

Measurements needed to determine the resistivity of a cylinder shaped conductor: $\rho = \frac{R\times Area}{length}$ where by Ohm's law the resistance $R = V/I$. An analogous experiment can be done for electrolyte solutions by filling a tube shaped container with electrolyte solution capped with metal electrodes on each end.
```

