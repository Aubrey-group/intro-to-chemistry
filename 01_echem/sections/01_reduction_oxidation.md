# 1.1 |  Reduction and Oxidation

**Word count:** <span id="word-count">0</span> words<br>**Reading Time:** <span id="read-time">0</span> minutes

**Electrochemistry** is the study of the the interactions between chemicals (molecules and materials) and electricity (exchange of charge as ions or electrons). A **redox reaction** is when electrons are exchanged between two chemical species. The species that loses electrons is call the **reducing agent (reductant)** and the species that gains electrons is called the **oxidizing agent (oxidant)**. One example of a redox reaction is shown in {numref}`oxidant-reductant`.


```{raw} html
:file: ../../assets/ECHEM/oxidant-reductant.svg
```
```{figure} ../../assets/ECHEM/oxidant-reductant.svg
:height: 0px
:name: oxidant-reductant

Redox reaction between copper(II) and iron(III) ions. Copper is reduced and iron is oxidized. Copper is the oxidant and iron is the reductant.
```

**Reduction** is the addition of electrons to a chemical species. **Oxidation** is the removal of electrons from a chemical species. A reaction in which a species is reduced is not possible without another species in the balance reaction being oxidized. This is because just like mass, charge is conserved in all chemical reactions. 

## A Periodic Trend: The Strength of Oxidants and Reductants

For the elements and elemental ions the *strength* of an oxidant typically increases with the elements *electronegativity*, {numref}`ptable-eneg-redox`. The most electronegative elements (*e.g.* Fluorine) are very strong oxidants and the most electropositive elements (*e.g.* Cesium) are very strong reductants. Strong oxidants easily accept electrons and strong reductants easily donate electrons. Strong oxidants are very poor reductants and strong reductants are very poor oxidants.

```{raw} html
:file: ../../assets/ECHEM/ptable-eneg-trend.svg
```
```{figure} ../../assets/ECHEM/ptable-eneg-trend.svg
:height: 0px
:name: ptable-eneg-redox

The trend of electronegativity across the periodic table. Eletronegativity increases from left to right and decreases from top to bottom. The most electronegative elements are strong oxidants and the most electropositive elements are strong reductants.
```

### Ionic Charge and Redox Strength

The oxidizing or reducing strength of an *elemental ion* is also influenced by the *ions charge*. For example, Fe<sup>2+</sup>  is a strong reductant than Fe<sup>3+</sup> because Fe<sup>3+</sup> has a a more negative charge and therefore more can easily donates electrons. Intuitively, the electrons in the more electron rich ion are less attracted to the nucleus of Fe<sup>2+</sup> because more electrons better screen the nuclear charge than in the case of the Fe<sup>3+</sup> ion. Therefore, the electrons are more weakly bound to the Fe<sup>2+</sup> ion and are more easily donated.

Likewise, Cu<sup>2+</sup> is a stronger oxidant compared to Cu<sup>+</sup> because Cu<sup>2+</sup> has a more positive charge that can more strongly attract electron from other chemical species. 


## Practice: Identifying oxidation and reduction in chemical reactions

1. For each of the following, identify the *strongest oxidant*.

<div id="flashcard2" class="flashcard" data-file="../../_static/stronger_oxidant.json" style="display:grid; grid-template-rows: 1fr auto; min-height:200px; margin:auto">
    <div id="content" class="content" style="display:flex;margin:auto;"></div>

<div style="display:flex;justify-content:center; align-items:center;">
<button id="prev" class="prev">Previous</button>
<button id="flip" class="flip">Flip</button>
<button id="next" class="next">Next</button>
</div>
</div>


2. For each of the following, identify the *strongest* *reductant*.

<div id="flashcard3" class="flashcard" data-file="../../_static/stronger_reductant.json" style="display:grid; grid-template-rows: 1fr auto; min-height:200px; margin:auto">
    <div id="content" class="content" style="display:flex;margin:auto;"></div>

<div style="display:flex;justify-content:center; align-items:center;">
<button id="prev" class="prev">Previous</button>
<button id="flip" class="flip">Flip</button>
<button id="next" class="next">Next</button>
</div>
</div>