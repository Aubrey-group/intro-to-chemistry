# Appendix 1: Table of Standard Reduction Potentials


$$
\begin{array}{rl}
    
F_2 + 2 e^{-}  & \leftrightharpoons 2 F^{-} \quad & E^{\circ} = 2.87 V\\
MnO_3(OH)^{-} + 3 H^{+} + 2 e^{-}  & \leftrightharpoons MnO_2 + 2 H_2O \quad & E^{\circ} = 2.09 V\\
O_3 + 2 H^{+} + 2 e^{-}  & \leftrightharpoons O_2 + H_2O \quad & E^{\circ} = 2.075 V\\
Ag^{+2} + 2 e^{-}  & \leftrightharpoons Ag \quad & E^{\circ} = 1.98 V\\
AgO + 2 H^{+} + e^{-}  & \leftrightharpoons Ag^{+} + H_2O \quad & E^{\circ} = 1.77 V\\
H_2O_2 + 2 H^{+} + 2 e^{-}  & \leftrightharpoons 2 H_2O \quad & E^{\circ} = 1.763 V\\
PbO_2 + SO_3(OH)^{-} + 3 H^{+} + 2 e^{-}  & \leftrightharpoons Pb(SO_4) + 2 H_2O \quad & E^{\circ} = 1.69 V\\
Pb^{+4} + 2 e^{-}  & \leftrightharpoons Pb^{+2} \quad & E^{\circ} = 1.69 V\\
Ag_2O_3 + 6 H^{+} + 6 e^{-}  & \leftrightharpoons 2 Ag + 3 H_2O \quad & E^{\circ} = 1.67 V\\
MnO_4^{-} + 8 H^{+} + 5 e^{-}  & \leftrightharpoons Mn^{+2} + 4 H_2O \quad & E^{\circ} = 1.51 V\\
PbO_2 + 4 H^{+} + 2 e^{-}  & \leftrightharpoons Pb^{+2} + 2 H_2O \quad & E^{\circ} = 1.46 V\\
Ce^{+4} + e^{-}  & \leftrightharpoons Ce^{+3} \quad & E^{\circ} = 1.44 V\\
Cl_2 + 2 e^{-}  & \leftrightharpoons 2 Cl^{-} \quad & E^{\circ} = 1.396 V\\
Cr_2O_7^{-2} + 14 H^{+} + 6 e^{-}  & \leftrightharpoons 2 Cr^{+3} + 7 H_2O \quad & E^{\circ} = 1.36 V\\
Tl^{+3} + 2 e^{-}  & \leftrightharpoons Tl^{+} \quad & E^{\circ} = 1.25 V\\
MnO_2 + 4 H^{+} + 2 e^{-}  & \leftrightharpoons Mn^{+2} + 2 H_2O \quad & E^{\circ} = 1.23 V\\
O_2 + 4 H^{+} + 4 e^{-}  & \leftrightharpoons 2 H_2O \quad & E^{\circ} = 1.229 V\\
MnO_2 + 4 H^{+} + e^{-}  & \leftrightharpoons Mn^{+3} + 2 H_2O \quad & E^{\circ} = 1.224 V\\
Ag_2O + 2 H^{+} + 2 e^{-}  & \leftrightharpoons 2 Ag + H_2O \quad & E^{\circ} = 1.17 V\\
Br_2 + 2 e^{-}  & \leftrightharpoons 2 Br^{-} \quad & E^{\circ} = 1.087 V\\
VO_2^{+} + 2 H^{+} + e^{-}  & \leftrightharpoons VO^{+2} + H_2O \quad & E^{\circ} = 1.0 V\\
NO_3^{-} + 3 H^{+} + 2 e^{-}  & \leftrightharpoons HNO_2 + H_2O \quad & E^{\circ} = 0.94 V\\
MnO_4^{-} + H^{+} + e^{-}  & \leftrightharpoons MnO_3(OH)^{-} \quad & E^{\circ} = 0.9 V\\
2 FeO_4^{-2} + 5 H_2O + 6 e^{-}  & \leftrightharpoons Fe_2O_3 + 10 OH^{-} \quad & E^{\circ} = 0.81 V\\
Ag^{+} + e^{-}  & \leftrightharpoons Ag \quad & E^{\circ} = 0.7996 V\\
Fe^{+3} + e^{-}  & \leftrightharpoons Fe^{+2} \quad & E^{\circ} = 0.771 V\\
BrO^{-} + H_2O + 2 e^{-}  & \leftrightharpoons Br^{-} + 2 OH^{-} \quad & E^{\circ} = 0.76 V\\
Tl^{+3} + 3 e^{-}  & \leftrightharpoons Tl \quad & E^{\circ} = 0.741 V\\
H_2SeO_3 + 4 H^{+} + 4 e^{-}  & \leftrightharpoons Se + 3 H_2O \quad & E^{\circ} = 0.74 V\\
H_2V_10O_28^{-4} + 24 H^{+} + 10 e^{-}  & \leftrightharpoons 10 VO(OH)^{+} + 8 H_2O \quad & E^{\circ} = 0.723 V\\
O_2 + 2 H^{+} + 2 e^{-}  & \leftrightharpoons H_2O_2 \quad & E^{\circ} = 0.695 V\\
H_2MoO_4 + 2 H^{+} + 2 e^{-}  & \leftrightharpoons MoO_2 + 2 H_2O \quad & E^{\circ} = 0.65 V\\
MnO_4^{-2} + 2 H_2O + 2 e^{-}  & \leftrightharpoons MnO_2 + 4 OH^{-} \quad & E^{\circ} = 0.6 V\\
S_2O_3^{-2} + 6 H^{+} + 4 e^{-}  & \leftrightharpoons 2 S + 3 H_2O \quad & E^{\circ} = 0.6 V\\
MnO_4^{-} + 2 H_2O + 3 e^{-}  & \leftrightharpoons MnO_2 + 4 OH^{-} \quad & E^{\circ} = 0.595 V\\
S_2O_6^{-2} + 4 H^{+} + 2 e^{-}  & \leftrightharpoons 2 H_2SO_3 \quad & E^{\circ} = 0.569 V\\
I_2 + 2 e^{-}  & \leftrightharpoons 2 I^{-} \quad & E^{\circ} = 0.5355 V\\
SO_2 + 4 H^{+} + 4 e^{-}  & \leftrightharpoons S + 2 H_2O \quad & E^{\circ} = 0.5 V\\
VO(OH)^{+} + 2 H^{+} + e^{-}  & \leftrightharpoons VOH^{+2} + H_2O \quad & E^{\circ} = 0.481 V\\
H_2SO_3 + 4 H^{+} + 4 e^{-}  & \leftrightharpoons S + 3 H_2O \quad & E^{\circ} = 0.45 V\\
H_2MoO_4 + 6 H^{+} + 3 e^{-}  & \leftrightharpoons Mo^{+3} + 4 H_2O \quad & E^{\circ} = 0.43 V\\
O_2 + 2 H_2O + 4 e^{-}  & \leftrightharpoons 4 OH^{-} \quad & E^{\circ} = 0.401 V\\
Cu^{+2} + 2 e^{-}  & \leftrightharpoons Cu \quad & E^{\circ} = 0.3419 V\\
VO^{+2} + 2 H^{+} + e^{-}  & \leftrightharpoons V^{+3} + H_2O \quad & E^{\circ} = 0.337 V\\
UO_2^{+} + 4 H^{+} + e^{-}  & \leftrightharpoons U^{+4} + 2 H_2O \quad & E^{\circ} = 0.273 V\\
PbO_2 + H_2O + 2 e^{-}  & \leftrightharpoons PbO + 2 OH^{-} \quad & E^{\circ} = 0.254 V\\
2 SO_4^{-2} + 4 H^{+} + 2 e^{-}  & \leftrightharpoons 2 H_2O + S_2O_6^{-2} \quad & E^{\circ} = 0.25 V\\
AgCl + e^{-}  & \leftrightharpoons Ag + Cl^{-} \quad & E^{\circ} = 0.22233 V\\
TiO^{+2} + 2 H^{+} + e^{-}  & \leftrightharpoons Ti^{+3} + H_2O \quad & E^{\circ} = 0.19 V\\
SO_4^{-2} + 4 H^{+} + 2 e^{-}  & \leftrightharpoons SO_2 + 2 H_2O \quad & E^{\circ} = 0.17 V\\
UO_2^{+2} + e^{-}  & \leftrightharpoons UO_2^{+} \quad & E^{\circ} = 0.163 V\\
Cu^{+2} + e^{-}  & \leftrightharpoons Cu^{+} \quad & E^{\circ} = 0.159 V\\
HSO_4^{-} + 3 H^{+} + 2 e^{-}  & \leftrightharpoons SO_2 + 2 H_2O \quad & E^{\circ} = 0.158 V\\
Sn^{+4} + 2 e^{-}  & \leftrightharpoons Sn^{+2} \quad & E^{\circ} = 0.154 V\\
S + 2 H^{+} + 2 e^{-}  & \leftrightharpoons H_2S \quad & E^{\circ} = 0.144 V\\
N_2H_4 + 4 H_2O + 2 e^{-}  & \leftrightharpoons 2 NH_4^{+} + 4 OH^{-} \quad & E^{\circ} = 0.11 V\\
H_2MoO_4 + 6 H^{+} + 6 e^{-}  & \leftrightharpoons Mo + 4 H_2O \quad & E^{\circ} = 0.11 V\\
HgO + H_2O + 2 e^{-}  & \leftrightharpoons Hg + 2 OH^{-} \quad & E^{\circ} = 0.0977 V\\
N_2 + 2 H_2O + 6 H^{+} + 6 e^{-}  & \leftrightharpoons 2 NH_4(OH) \quad & E^{\circ} = 0.092 V\\
S_4O_6^{-2} + 2 e^{-}  & \leftrightharpoons 2 S_2O_3^{-2} \quad & E^{\circ} = 0.08 V\\
AgBr + e^{-}  & \leftrightharpoons Ag + Br^{-} \quad & E^{\circ} = 0.07133 V
\end{array}
$$

$$
\begin{array}{rl}
2 H^{+} + 2 e^{-}  & \leftrightharpoons H_2 \quad & E^{\circ} = 0 V
\end{array}
$$

$$
\begin{array}{rl}
P + 3 H^{+} + 3 e^{-}  & \leftrightharpoons PH_3 \quad & E^{\circ} = -0.063 V\\
VOH^{+2} + H^{+} + e^{-}  & \leftrightharpoons V^{+2} + H_2O \quad & E^{\circ} = -0.082 V\\
SnO_2 + 2 H^{+} + 2 e^{-}  & \leftrightharpoons SnO + H_2O \quad & E^{\circ} = -0.088 V\\
WO_3 + 6 H^{+} + 6 e^{-}  & \leftrightharpoons W + 3 H_2O \quad & E^{\circ} = -0.09 V\\
SnO + 2 H^{+} + 2 e^{-}  & \leftrightharpoons Sn + H_2O \quad & E^{\circ} = -0.104 V\\
WO_2 + 4 H^{+} + 4 e^{-}  & \leftrightharpoons W + 2 H_2O \quad & E^{\circ} = -0.12 V\\
Pb^{+2} + 2 e^{-}  & \leftrightharpoons Pb \quad & E^{\circ} = -0.126 V\\
Sn^{+2} + 2 e^{-}  & \leftrightharpoons Sn \quad & E^{\circ} = -0.14 V\\
MoO_2 + 4 H^{+} + 4 e^{-}  & \leftrightharpoons Mo + 2 H_2O \quad & E^{\circ} = -0.15 V\\
AgI + e^{-}  & \leftrightharpoons Ag + I^{-} \quad & E^{\circ} = -0.15224 V\\
Ni^{+2} + 2 e^{-}  & \leftrightharpoons Ni \quad & E^{\circ} = -0.257 V\\
Ni^{+2} + 2 e^{-}  & \leftrightharpoons Ni \quad & E^{\circ} = -0.257 V\\
V^{+3} + e^{-}  & \leftrightharpoons V^{+2} \quad & E^{\circ} = -0.26 V\\
N_2 + 8 H^{+} + 6 e^{-}  & \leftrightharpoons 2 NH_4^{+} \quad & E^{\circ} = -0.27 V\\
PO(OH)_3 + 2 H^{+} + 2 e^{-}  & \leftrightharpoons HPO(OH)_2 + H_2O \quad & E^{\circ} = -0.276 V\\
Tl^{+} + e^{-}  & \leftrightharpoons Tl \quad & E^{\circ} = -0.34 V\\
Pb(SO_4) + 2 e^{-}  & \leftrightharpoons Pb + SO_4^{-2} \quad & E^{\circ} = -0.356 V\\
Cu_2O + H_2O + 2 e^{-}  & \leftrightharpoons 2 Cu + 2 OH^{-} \quad & E^{\circ} = -0.36 V\\
Ti^{+3} + e^{-}  & \leftrightharpoons Ti^{+2} \quad & E^{\circ} = -0.37 V\\
Cd^{+2} + 2 e^{-}  & \leftrightharpoons Cd \quad & E^{\circ} = -0.403 V\\
Cr^{+3} + e^{-}  & \leftrightharpoons Cr^{+2} \quad & E^{\circ} = -0.424 V\\
Fe^{+2} + 2 e^{-}  & \leftrightharpoons Fe \quad & E^{\circ} = -0.44 V\\
Bi_2O_3 + 3 H_2O + 6 e^{-}  & \leftrightharpoons 2 Bi + 6 OH^{-} \quad & E^{\circ} = -0.452 V\\
HPO(OH)_2 + 3 H^{+} + 3 e^{-}  & \leftrightharpoons P + 3 H_2O \quad & E^{\circ} = -0.454 V\\
HPO(OH)_2 + 2 H^{+} + 2 e^{-}  & \leftrightharpoons H_2PO(OH) + H_2O \quad & E^{\circ} = -0.499 V\\
H_2PO(OH) + H^{+} + e^{-}  & \leftrightharpoons P + 2 H_2O \quad & E^{\circ} = -0.508 V\\
U^{+4} + e^{-}  & \leftrightharpoons U^{+3} \quad & E^{\circ} = -0.52 V\\
GaO(OH)_2^{-} + H_2O + 3 e^{-}  & \leftrightharpoons Ga + 4 OH^{-} \quad & E^{\circ} = -0.56 V\\
2 TiO_2 + 2 H^{+} + 2 e^{-}  & \leftrightharpoons Ti_2O_3 + H_2O \quad & E^{\circ} = -0.56 V\\
PbO + H_2O + 2 e^{-}  & \leftrightharpoons Pb + 2 OH^{-} \quad & E^{\circ} = -0.58 V\\
Ag_2S + 2 e^{-}  & \leftrightharpoons 2 Ag + S^{-2} \quad & E^{\circ} = -0.69 V\\
Ni(OH)_2 + 2 e^{-}  & \leftrightharpoons Ni + 2 OH^{-} \quad & E^{\circ} = -0.72 V\\
Zn^{+2} + 2 e^{-}  & \leftrightharpoons Zn \quad & E^{\circ} = -0.7618 V\\
2 H_2O + 2 e^{-}  & \leftrightharpoons H_2 + 2 OH^{-} \quad & E^{\circ} = -0.8277 V\\
Fe_2O_3 + 3 H_2O + 2 e^{-}  & \leftrightharpoons 2 Fe(OH)_2 + 2 OH^{-} \quad & E^{\circ} = -0.86 V\\
Fe(OH)_2 + 2 e^{-}  & \leftrightharpoons Fe + 2 OH^{-} \quad & E^{\circ} = -0.89 V\\
Cr^{+2} + 2 e^{-}  & \leftrightharpoons Cr \quad & E^{\circ} = -0.9 V\\
TiO^{+2} + 2 H^{+} + 4 e^{-}  & \leftrightharpoons Ti + H_2O \quad & E^{\circ} = -0.93 V\\
Sn + 4 H^{+} + 4 e^{-}  & \leftrightharpoons SnH_4 \quad & E^{\circ} = -1.07 V\\
V^{+2} + 2 e^{-}  & \leftrightharpoons V \quad & E^{\circ} = -1.13 V\\
Mn^{+2} + 2 e^{-}  & \leftrightharpoons Mn \quad & E^{\circ} = -1.185 V\\
Ti_2O_3 + 2 H^{+} + 2 e^{-}  & \leftrightharpoons 2 TiO + H_2O \quad & E^{\circ} = -1.23 V\\
B(OH)_4^{-} + 4 H_2O + 8 e^{-}  & \leftrightharpoons BH_4^{-} + 8 OH^{-} \quad & E^{\circ} = -1.24 V\\
TiO + 2 H^{+} + 2 e^{-}  & \leftrightharpoons Ti + H_2O \quad & E^{\circ} = -1.31 V\\
Ti^{+3} + 3 e^{-}  & \leftrightharpoons Ti \quad & E^{\circ} = -1.37 V\\
Ti^{+2} + 2 e^{-}  & \leftrightharpoons Ti \quad & E^{\circ} = -1.63 V\\
Al^{+3} + 3 e^{-}  & \leftrightharpoons Al \quad & E^{\circ} = -1.676 V\\
HPO_3^{-2} + 2 H_2O + 3 e^{-}  & \leftrightharpoons P + 5 OH^{-} \quad & E^{\circ} = -1.71 V\\
BO(OH)_2^{-} + H_2O + 3 e^{-}  & \leftrightharpoons B + 4 OH^{-} \quad & E^{\circ} = -1.79 V\\
U^{+3} + 3 e^{-}  & \leftrightharpoons U \quad & E^{\circ} = -1.798 V\\
H_2PO_2^{-} + e^{-}  & \leftrightharpoons P + 2 OH^{-} \quad & E^{\circ} = -1.82 V\\
Be^{+2} + 2 e^{-}  & \leftrightharpoons Be \quad & E^{\circ} = -1.99 V\\
Ac^{+3} + 3 e^{-}  & \leftrightharpoons Ac \quad & E^{\circ} = -2.2 V\\
Al(OH)_3 + 3 e^{-}  & \leftrightharpoons Al + 3 OH^{-} \quad & E^{\circ} = -2.3 V\\
Be_2O_3^{-2} + 3 H_2O + 4 e^{-}  & \leftrightharpoons 2 Be + 6 OH^{-} \quad & E^{\circ} = -2.63 V\\
Mg(OH)_2 + 2 e^{-}  & \leftrightharpoons Mg + 2 OH^{-} \quad & E^{\circ} = -2.69 V\\
Ba(OH)_2 + 2 e^{-}  & \leftrightharpoons Ba + 2 OH^{-} \quad & E^{\circ} = -2.99 V\\
Ca(OH)_2 + 2 e^{-}  & \leftrightharpoons Ca + 2 OH^{-} \quad & E^{\circ} = -3.02 V\\
Li^{+} + e^{-}  & \leftrightharpoons Li \quad & E^{\circ} = -3.04 V\\
N_2 + 4 H_2O + 2 e^{-}  & \leftrightharpoons 2 NH_2OH + 2 OH^{-} \quad & E^{\circ} = -3.04 V
\end{array}
$$

<!-- Data collected from libretext and wikipedia -->