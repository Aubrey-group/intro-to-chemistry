# 1.9 | Potential of Half Reactions

**Word count:** <span id="word-count">0</span> words<br>**Reading Time:** <span id="read-time">0</span> minutes

We have seen that for any redox reaction, a redox potential can be defined in relation to Gibb's free energy of reaction $\Delta G$ by converting from units of joules per mole reaction to joules per mol electron transferred then converting to the SI units of volts (joules per couloumb). That is, 
$$E = \frac{-\Delta G}{nF}$$ 
where $n$ is the number of electrons transferred and $F$ is Faraday's constant. 

While we could tabulate the potential of every possible redox reaction, just as we saw in thermochemistry with heats of reaction, we can greatly reduce the number of reaction we need to consider by measuring potentials with respect to a standard reference electrode.

For example in the zinc-copper cell, if we allow no current to flow between the electrodes (*i.e.* open circuit), the potential of the zinc electrode is measured to be $E_{cell}^\circ=1.10\,V$. The variable <b>Eº<sub>cell</sub></b> signifies the voltage of an electrochemical cell measured at zero current. At equilibrium this is the *thermodynamic redox potential of the cell*.

We could also measure the thermodynamic redox potential of a zinc-tin cell to be 0.62 V. And we could also measure the thermodynamic redox potential of a copper-tin. However,  we can simply calculate the redox potential for the copper-tin cell from the known redox potentials of the zinc-copper and zinc-tin.

$$
|\, Zn \,| \,ZnSO_4\,(aq)\,|\, SnSO_4\,(aq)\,|\, Sn \,|\quad Eº_{cell}= 0.62\,V\\
|\, Zn \,| \,ZnSO_4\,(aq)\,|\, CuSO_4\,(aq)\,|\, Cu \,|\quad Eº_{cell}= 1.10\,V
$$

By expressing these electrochemical cells balanced redox reactions, using Hess's law, the two reactions can be combined to give the net redox reaction for the copper-tin cell.

$$
\,\,\,\,1 \times \quad Zn(s) + Cu^{2+}(aq) \rightarrow Zn^{2+}(aq) + Cu(s) \quad Eº_{cell}= 1.10\,V\\
+ -1 \times \quad Zn(s) + Sn^{2+}(aq) \rightarrow Zn^{2+}(aq) + Sn(s)  \quad Eº_{cell}= 0.62\\ 
\hline 
\,\,\quad \cancel{Zn(s)} + Cu^{2+}(aq) \rightarrow \cancel{Zn^{2+}(aq)} + Cu(s) \quad Eº_{cell}= 1.10\,V \\
+ \quad \cancel{Zn^{2+}(aq)} + Sn(s) \rightarrow \cancel{Zn(s)} + Sn^{2+}(aq) \quad Eº_{cell}= -0.62\,V \\ \hline
{\color{var(--pst-color-primary)} Sn(s) + Cu^{2+}(aq) \rightarrow Sn^{2+}(aq) + Cu(s) \quad Eº_{cell}= 0.48\,V}
$$

Here, we have effectively used zinc as a **reference electrode**. More specifically, we have use the half reaction $Zn(s) \rightarrow Zn^{2+}(aq) + 2e^-$ as a common reference to measure the potentials of the half reactions:

$$
Cu^{2+}(aq) + 2e^- \rightarrow Cu(s) \quad Eº_{cell}= 1.10\,V\,vs\, Zn/Zn^{2+}\\
Sn^{2+}(aq) + 2e^- \rightarrow Sn(s) \quad Eº_{cell}= 0.62\,V\,vs\, Zn/Zn^{2+}
$$

## Standard Hydrogen Electrode

In practice, Zn is not used as a common reference electrode. Instead, the **standard hydrogen electrode (SHE)** is the most common reference electrode. Tables of the potentials of half reactions are usually given with respect to the SHE unless otherwise specified.

 The SHE ({numref}`SHE-electrode`) is a platinum electrode coated with platinum black and immersed in a solution of 1 M HCl. Hydrogen gas is bubbled through the solution at 1 atm pressure. The half reaction for the SHE electrode is:

$$
2H^+ + 2e^- \rightarrow H_2(g) \quad Eº_{cell}= 0.00\,V\,vs\, SHE
$$

In the standard hydrogen electrode the Pt electrode does not participate in the net reaction as either a reactant or product. Platinum only mediate the transfer of electrons to solution where hydronium ions are reduced to form hydrogen gas dissolved in solution. So long as the concentration of $H^+$ is 1 M and the pressure of $H_2$ is 1 atm, the potential of the SHE is stable at 0.00 V. The SHE is just one of many electrochemical reactions were the species that is reduced is not part of the electrode but rather a species dissolved in solution.  

```{raw} html
:file: ../../assets/ECHEM/SHE.svg
```

```{figure} ../../assets/logo-dark.svg
:height: 0px
:name: SHE-electrode

The net transfer of charge as electrons and ios within the galvanic zinc-copper cell with a zinc sulfate electrolyte at the anode and a copper sulfate electrolyte at the cathode. Electrons flow from the anode to the cathode through the external circuit. Negatively charged, sulfate ions (SO<sub>4</sub><sup>2–</sup>) flow from the cathode to the anode through the electrolyte. 
```

## Determining the Voltage of an Electrochemical Cell

The open circuit potential of a cell, $Eº$, is the potential of the cell when no current is flowing. If the standard reduction potentials of the two half reactions are known, then the voltage of the cell can be calculated as:

$$
Eº_{cell} = Eº_{cathode} - Eº_{anode}
$$

This expression is merely a shorthand of Hess's Law that allows use to combine multiple reactions in a system to determine the net reaction. **NOTE**: Unlike in thermochemistry where $\Delta H$ and $\Delta G$ were measured in kJ per mole or *reaction*, in electrochemistry $E$ is measured in V or J per coulomb. If a half reaction is reversed, the sign of the potential is inverted but if a half reaction is multiplied by a factor other than 1, the potential is **not** multiplied by that factor. For example below are the standard (ions are 1 M concentration, gasses are at 1 atm) half-reactions for the zinc-copper cell referenced to the SHE:

$$
Zn^{2+}(aq) + 2e^- \rightarrow Zn(s) \quad Eº_{cell}= -0.76\,V\\
Cu^{2+}(aq) + 2e^- \rightarrow Cu(s) \quad Eº_{cell}= 0.34\,V
$$

Applying the formula above, the voltage of the zinc-copper cell is: 

$$
Eº_{cell} = Eº_{cathode} - Eº_{anode} = 0.34\,V - (-0.76\,V) = 1.10\,V
$$

In the case of the zinc half reaction, 

$$
\begin{array}{rl}
Zn^{2+}(aq) + 2e^- & \rightarrow Zn(s) \quad &Eº_{cell}= -0.76\,V \\
Zn(s) & \rightarrow Zn^{2+}(aq) + 2e^- \quad &Eº_{cell}= +0.76\,V\text{ (reverse)} \\
2Zn(s) & \rightarrow 2Zn^{2+}(aq) + 4e^- \quad &Eº_{cell}= +0.76\,V\text{ (multiply by 2x)} \\
\end{array}
$$

```{admonition} Standard Conditions for Electrochemical Cells
:class: note

The standard conditions for electrochemical cells are:

- All ions are 1 M aqueous solutions.
- All gases are at 1 atm pressure.
- The temperature is 25°C (298 K).
- Solids are pure and in their standard state.
- The potential of the standard hydrogen electrode is 0.00 V. All other potentials are measured relative to the SHE.
```


## Spontaneity of Electrochemical Cells

The spontaneity of a redox reaction can be determined by the sign of the cell potential.

**Galvanic cells** are electrochemical cells in which a spontaneous redox reaction will occur. That is $Eº_{cell} > 0$ which is the same as $\Delta G < 0$. In a galvanic cell as the electrochemical cell proceeds, work in the form of electrical energy is produced. Galvanic cells are useful for storing and generating electrical energy. The zinc-copper cell and a Lithium-ion battery are examples of a galvanic cells.



```{raw} html
:file: ../../assets/ECHEM/electrolysis-water.svg
```

```{figure} ../../assets/logo-dark.svg
:height: 0px
:name: electrolysis-water

The electrolysis of water to produce hydrogen and oxygen gas. The electrolysis of water is a non-spontaneous redox reaction that can be driven by applying an external source of electrical potential. Protons are reduced at the cathode to form hydrogen gas while hydroxide ions are oxidized at the anode to form oxygen gas. An external voltage source is required to drive reaction and evolve the gases at the cathode and anode.  
```

**Electrolytic cells** are cells in which the redox reaction is *not* spontaneous redox . That is $Eº_{cell} < 0$ which is the same as $\Delta G > 0$. In an electrolytic cell, work in the form of electrical energy is consumed to drive a non-spontaneous redox reaction. By applying an external source of electrical potential, say from an electric  generator, the reaction can be driven to become spontaneous and proceed in the forward direction. Electrolytic cells are useful for making higher value chemical compounds, purifying metals, and modifying surfaces in manufactures (e.g. electroplating, passivization, electrochemical machining, ...). An example of an electrolytic cell is the electrolysis of water to produce hydrogen and oxygen gas, {numref}`electrolysis-water`.

## Table of Standard Reduction Potentials

The potentials of half reactions are usually tabulated as *standard reduction potentials*, {numref}`half-rxns`. When doing so the reactants contain the oxidant (species being reduced by accepting electrons). The products of the half reactions in {numref}`half-rxns` are chemical reductants if these half reactions are reversed then the standard oxidation potential of these chemical reductants is simply the multiplicative inverse of the listed reduction potential.

```{raw} html
:file: ../../assets/ECHEM/half-rxns.svg
```

```{figure} ../../assets/logo-dark.svg
:height: 0px
:name: half-rxns

The standard reduction potentials for half reactions tabulated with respect to the SHE. Chemical oxidants are listed as reactants (species that accept electrons) and chemical reductants are listed as products (species that donate electrons). When the potentials listed from most positive to most negative from top to bottom, the oxidants (reactants) are decreasing in strength and the reductants (products) are increasing in strength. 
```