# 1.10 | E-pH Diagrams

**Word count:** <span id="word-count">0</span> words<br>**Reading Time:** <span id="read-time">0</span> minutes


The Nernst Equation provides access to another powerful thinking tool in electrochemistry that allows us to analyze the relative stabilities of different species as a function of electrochemical potential and pH. Termed E-pH diagrams (aka Pourbaix diagrams), the nernst equation can be rearranged to include pH as a variable. Take for example the reduction of protons in water to form hydrogen gas:



$$
2 H^+ + 2 e^- \rightarrow H_2 \\[2em]
$$

$$
\begin{align*}
E &= E^\circ - \frac{RT}{n_eF} \ln(Q) \quad \text{(Nernst Equation)} \\
&\quad Q = \frac{[H_2]}{[H^+]}\\
E &= E^\circ + \frac{RT}{n_eF} \ln\left(\frac{[H_2]}{[H^+]}\right) \\
&\quad pH = -\log_{10}\left([H^+]\right) \quad \text{(definition of pH)} \\[1em]
&\quad \text{convert } \ln \text{ to } \log_{10} \text{ and consolidate constants. Assume } T=298\,K\\
E &= E^\circ - \left(\frac{59.2\,mV}{n_e}\right) \log_{10}([H_2]) -  \left(\frac{-59.2\,mV\,n_{H^+}}{n_e}\right) \log_{10}([H^+])  \\
&\quad \text{substitute } [H^+] \text{ for } pH\\
E &= E^\circ - \left(\frac{59.2\,mV}{n_e}\right) \log_{10}([H_2]) -  \left(\frac{+59.2\,mV\,n_{H^+}}{n_e}\right) pH  \\
&\quad \text{where } E^\circ=0.00,\, n_{H^+} = 2, \text{ and }  n_{e} = 2 \text{ for the proton reduction reaction.} \\
E &= 0.00\,V - \left(\frac{59.2\,mV}{2}\right) \log_{10}([H_2]) -  \left(59.2\,mV \right) pH  \\
E &= \left(\frac{59.2\,mV}{2}\right) \log_{10}([H_2]) -  \left(59.2\,mV \right) pH  \\
\end{align*}
$$

If the concentration of H<sub>2</sub> is constant then this final equation is a liner relationship of the form $y=mx+b$ where the slope is $59.2\,mV$ and the y-intercept is $0.00\,V$. 
For any generic half reaction the pH dependence can be expressed as:

<div style="border: 1px solid black; padding: 10px; margin: 10px; background-color: var(--pst-color-info-bg);">

$$
\begin{align*}    
E &= E^\circ - \left(\frac{59.2 mV}{n_e}\right) \left(\log_{10}(Q^\prime) +  n_{H^+} pH\right)  \\
&\quad \text{or}\\
E &= \text{const} - \left(\frac{59.2\,mV\,n_{H^+}}{n_e}\right)pH
\end{align*}

$$

</div>

where $n_{H^+}$ is the number of protons in the half reaction and $Q^\prime$ is the reaction quotient with the concentration of protons removed.

## Visualizing the Nernst Equation using E-pH Diagrams

Shown in {numref}`pourbaix-diagram-water` is the E-pH diagram for water. Here the lower dashed line represents the potential for proton reduction to hydrogen gas. Here we should note a few important observations:

1. Starting at an acidic pH, the potential for proton reduction decreases as pH increases—that is, as the solution becomes more basic.
2. A pH's greater than 7 hydroxide is the dominant species and the relevant half reaction becomes the reduction of water to H<sub>2</sub> and hydroxide. 
3. Despite the change in the half reaction form containing H<sup>+</sup> to OH<sup>-</sup>, the slope of the line remains constant and the potential for the reaction as a function of pH is a smooth and continuous line. Recall the autoionization of water and the fixed relationship between the concentration of protons and hydroxide ions in water. 
4. The slope of the line in the E-pH diagram will always be $\frac{59.2\,mV\,n_{H^+}}{n_e}$ where $n_{H^+}$ is the number of protons and $n_e$ is the number of electrons in the half reaction balance under acidic conditions expressed as a reduction.

```{raw} html
:file: ../../assets/ECHEM/H_pourbaix.svg
```

```{figure} ../../assets/logo-dark.svg
:height: 0px
:name: pourbaix-diagram-water

E-pH diagram for water. Dashed lines represent the potential of O<sub>2</sub> reduction (top) and water reduction (bottom). The E-pH diagram for water is superimposed onto every other E-pH diagram indicating the stability limits of solvent. 
```

The upper dashed line in {numref}`pourbaix-diagram-water` represents the potential for the oxygen reduction reaction. Note in this chase 4 electrons and 4 protons appear in the balanced reduction reaction. Therefore, the slope of this line is also 59.2 mV/pH and runs parallel to the proton reduction line. 

Other reactions will have different slopes but the can always be calculated as $\text{slope} = \frac{59.2\,mV\,n_{H^+}}{n_e}$. If the reaction in question is purely reduction (no protons) then line will be horizontal (slope = 0). If the reaction is only an acid-base reaction and no electrons appear in the balanced reaction the line in the E-pH diagram will be vertical.


### E-pH diagrams for the elements

While the E-pH diagram for water is quite simple,  with only two possible reactions shown under normal conditions, the E-pH diagrams of the elements display a variety of acid-base reactions, can access multiple oxidation states of the element. For example, in {numref}`pourbaix-diagram` is shown the E-pH diagram for copper. At very oxidizing potentials and very acidic conditions the most stable species of copper is Cu<sup>2+</sup> as shown in the top right corner. The bounding region of the E-pH diagram in which the Cu<sup>2+</sup> label is located indicates that *copper is most stable as Cu<sup>2+</sup>  and Cu<sup>2+</sup> (*aq*) is the predominant species of copper in solution. 



```{raw} html
:file: ../../assets/ECHEM/Cu_pourbaix.svg
```

```{figure} ../../assets/logo-dark.svg
:height: 0px
:name: pourbaix-diagram

E-pH diagram for copper at 0.1 M copper dissolved in water. Dashed lines represent the potential of water oxidation (top) and water reduction (bottom). (Data Source[^1])
```

At pH = 1 and moving to more reducing potentials, say from E = 2 V to E = -1 V, we cross a horizontal line that forms the border of the Cu<sup>2+</sup> region and the Cu (s) region. This line is defined by the reduction potential of the reaction $Cu^{2+} \rightarrow Cu (s)$ which balance we can see is a pure reduction reaction with no protons in the balanced reaction: $Cu^{2+} + 2e^- \rightarrow Cu (s)$. Therefore the slope of this line is zero and the line is horizontal.

Again starting as pH = 1 and E = 2 V and moving to higher pH values we cross a vertical line that forms the border of the Cu<sup>2+</sup> region and the Cu(OH)<sub>2</sub> region. This line is defined by the *pKa* for the pure acid base reaction $Cu^{2+} + 2OH^- \rightarrow Cu(OH)_2$. This is *not* a redox reaction but a pure *acid-base* reaction. Because no electrons are present in the balanced reaction this line is vertical. At E = 2 V and pH = 10 Cu(OH)<sub>2</sub> (*s*) is the most stable and predominant species of copper. Not only is this an acid-base reaction but the change in phase from aqueous to solid defines this as a *precipitation* reaction as well. 


## Corrosion and Passivation

On the E-pH diagram above we can also identity regions where copper metal is and is not stable in aqueous solutions. In areas where a dissolved species is predominant, is the elemental solid is exposed to these conditions then it will undergo **corrosion**. Corrosion is the oxidative dissolution of a metal into solution. In the case of copper, corrosion occurs at low pH and potentials greater than +0.34 V. At basic pH and potentials greater than about 0.0 to +0.25 V, copper is also expected to oxidize to Cu<sub>2</sub>O (*s*). If this oxidized copper solid forms a protective layer on the surface of copper then after a show period of the the oxidation reaction will slow or stop completely as the oxide layer protects the underlying copper metal from the aqueous solution. This process is called **passivation** and is a common basis for the corrosion resistance of many metals in the presents of air and water (e.g. aluminum metal). Because this passivation occurs via oxidation or at the anode of an electrochemical cell it is sometimes more precisely called **anodic passivation**.

There is an alternative means of preventing metals from corrosion that appears on the E-pH diagram and that is simply to keep the pH and the potential of the metal in a region where the metal is stable. This is called **cathodic protection** which maintains a pristine metal surface in a reducing environment. Only some metals can be protected in this way. If the reduction potential of the metal is below the reduction potential to form hydrogen gas then the application of a reducing potential will either evolve hydrogen gas or if the metal does form it is thermodynamically favorable for the metal then corrode and evolve hydrogen gas. 

Cathodic protection can be accomplished in two common ways. One is to directly apply a reducing potential powered by an eternal electric generator (i.e. a power plant). This is called **impressed current cathodic protection** and is commonly used to protect buried iron or copper pipe works that are difficult to regularly access. The other perhaps simpler method is to attach a more reducing and reactive metal to the metal that is to be protected. This is called **galvanic cathodic protection** and is commonly used to protect the iron hulls of ships from rusting at sea. 


## Practice

1. Prove that the Nernst Equation for the reaction $H_2O + 2e^- \rightarrow H_2 + 2OH^-$ ($E^\circ =-0.829$) is equivalent to that for the reaction $2 H^+ + 2e^- \rightarrow H_2$ ($E^\circ =0.0$).

```{dropdown} Solution
\begin{align*}
E = E^\circ_{OH^-} - \frac{RT}{n_eF} \ln(Q)  = E^\circ_{OH^-} - \frac{RT}{n_eF} \ln\left([H_2][OH^-]^{n_{OH^-}}[H_2]\right) \\
\end{align*}

converting to log base 10 and reformatting the log term as a sum of two terms:

$$
E &= E^\circ_{OH^-} - \left(\frac{59.2 mV}{n_e}\right) \log_{10}([H_2]) -  \left(\frac{59.2 mV\,n_{OH^-}}{n_e}\right) \log_{10}([OH^-])\\ 
E &= E^\circ_{OH^-} - \left(\frac{59.2 mV}{n_e}\right) \log_{10}([H_2]) +  \left(\frac{59.2 mV\,n_{OH^-}}{n_e}\right) pOH\\ 
E &= E^\circ_{OH^-} - \left(\frac{59.2 mV}{n_e}\right) \log_{10}([H_2]) +  \left(\frac{59.2 mV\,n_{OH^-}}{n_e}\right) (14-pH)\\ 
$$

for this particular reaction $n_{OH^-} = 2$ and $n_{H^+} = 2$ so the equation simplifies to:

$$
E &= E^\circ_{OH^-} - \left(\frac{59.2 mV}{2}\right) \log_{10}([H_2]) + 0.829 V -  \left(59.2 mV \right)pH\\ 
$$

Given $E^\circ_{OH^-} = -0.829\,V$, the equation simplifies to:

$$
E = 0 -  \left(\frac{59.2 mV}{2}\right) \log_{10}([H_2]) -  \left(59.2 mV \right)pH
$$

This is equivalent to the Nernst equation for the reaction $2 H^+ + 2e^- \rightarrow H_2$ show above. 
```

2. Describe an experiment that would allow you to convert copper hydroxide (*s*) to Cu (s).  

```{dropdown} Solution

Titrate the copper hydroxide with strong acid to a pH < 3 to dissolve the copper hydroxide as Cu<sup>2+</sup> (*aq*). Then either

1. Apply a potential to a copper electrode of less than +0.34 V to reduce the Cu<sup>2+</sup> to Cu (s)
2. or add a suitable reducing agent (*e.g.* Ti<sup>2+</sup>) to the solution to reduce the Cu<sup>2+</sup> to Cu (s). 

```

3. What is the balance reaction that defined the border between Cu(OH)<sub>2</sub> and Cu<sub>2</sub>O?

```{dropdown} Solution

$$
2 Cu(OH)_2 + 2 e^- \rightarrow Cu_2O + 2 OH^- + H_2O
$$

We could also write this reaction under acidic conditions if we choose to conduct the reaaction between pH = 5 and pH = 7.

$$
2 Cu(OH)_2 + 2 e^- + 2 H^+ -> 1 Cu_2O + 3 H_2O
$$

Note the ratio of protons to electrons is in the acidic reaction is the same as the ratio of hydroxide ions to electrons in the basic reaction. These two reactions describe the same Nernst equation. 
```

## References

[^1]: Schweitzer, G. K. & Pesterfield L. L *The Aqueous Chemistry of the Elements* **2010**, Oxford University Press.