# 1.2 | Oxidation states 

**Word count:** <span id="word-count">0</span> words<br>**Reading Time:** <span id="read-time">0</span> minutes<br>
**Slide Deck**: [ECHEM-1](../../assets/ECHEM/D1.pdf)

Within chemical compounds it is often useful to assign a formal oxidation state to each element. While this values do not reflect a quantitative measure of charge at a given atom, they are extremely useful for (1) understanding the distribution of electrons within a molecule, (2) classifying like atoms within and between compounds, (3) counting the number of electrons in a compound and importantly (4) accounting for the exchange of electrons between compounds in redox reactions. Later we'll also see their important use in predicting the stability, color, and magnetism of transition metal-based compounds as well.

When faced with a new chemical structure or molecular formula the first thing a chemist will usually do is assign the formal oxidations states. This becomes a habit so automatic that it is often seemingly done without thinking. While we can easily assign an oxidation state to nearly every atom in every compound, it is important to remember that *oxidation states are not real ionic charges*. Usually the oxidation state is an integer value between -4 and +7. Oxidation states can be equal to zero—as in the case in the elements and other compounds. Sometimes oxidation states are even fractions when electrons are delocalized across multiple atoms (*e.g.* as in superoxide: $OS_O = -\frac{1}{2}$ in O<sub>2</sub><sup>-1</sup>).

## Assigning oxidation states

The rules for assigning oxidation states can be described algorithmically. However, it is important to know these rules are empirical and based upon "ionizing" polar covalent bonds but giving electrons to the more electronegative atom and achieve closed shell electron configurations when possible. 

### Step 1: Try to make the easy assignments first

- Free, uncompounded elements: OS = zero.
- Monoatomic ions: OS = charge.
- Group 1 metals (*e.g.* Na, K, Li, ...): OS = +1.
- Group 2 metals (*e.g.* Mg, Ca, Ba, ...): OS = +2.
- F in a compound: OS = -1

### Step 2: Try to make an assignment for H and O

- H when bonded to non-metals: OS = +1 (usually)
- H when bonded to metals: OS = -1 (a "metal hydride")
- Oxygen being very electron negative: O = -2 (usually)
- The oxidation state is increased by 1 for each bond oxygen makes to itself (*e.g.* in peroxide $H–O–O–H$: $OS_O = -1$)
- The last rule is generally true for other elements as well (*e.g.* hydrazine $H_2N-NH_2$: $OS_N = -2,\,OS_H=+1$)

### Step 3: Work on the others by considering the sum of oxidation states

- The sum of the oxidation states in a neutral compound = zero.
- The sum of the oxidation states in an ion = charge of ion.

### Step 4: If needed, attempt a best guess based on electronegativity

In some cases we may need to make an educated guess then check to make sure all the final assignments are physically reasonable. Note the many of the most interesting compounds are interesting because of their unusual oxidations states.

- More electronegative atoms assigned negative oxidation states.
- Less electronegative atoms assigned positive oxidation states.
- Many *s*-block and *p*-block elements have predictable oxidation states based on the formation of closed shell electron configurations. This is generally not true for the *d*-block elements.
- If a compound includes a recognizable polyatomic ion you can assign the oxidation state for the entire ion as a group which is simply the formal charge of the ion. This can make it a bit easier to assign oxidations states to transition metals that can take on many different oxidation states in stable compounds. For example, in $Fe(SO_4)$ the sulfate ion $SO_4^{2-}$ has a charge of -2 therefore for charge balance $OS_{Fe} = +2$. We can also assign the oxidation states for atoms within $SO_4^{2-}$ as $OS_S = +6$ and $OS_O = -2$ but if we only want to know the oxidation state of the iron we can simplify the process by just using the polyatomic ion's group charge.

## Practice: Assigning oxidation states

Assign the formal oxidation states for the elements in the following compounds. If a common polyatomic ion is present, assign the formal charge for the polyatomic ion as well. 

<!-- <div id="flashcard" class="flashcard" data-file="../../_static/flashcards.json">
    <div id="content"></div>
</div>

<div style="display:flex; justify-content:center; align-items:center; margin:auto">
<button id="prev">Previous</button>
<button id="flip">Flip</button>
<button id="next">Next</button>
</div> -->


<div id="flashcard2" class="flashcard" data-file="../../_static/flashcards.json" style="display:grid; grid-template-rows: 1fr auto; min-height:200px; margin:auto">
    <div id="content" class="content" style="display:flex;margin:auto;"></div>

<div style="display:flex;justify-content:center; align-items:center;">
<button id="prev" class="prev">Previous</button>
<button id="flip" class="flip">Flip</button>
<button id="next" class="next">Next</button>
</div>
</div>



## Beyond the basics

Some compounds have atoms in multiple oxidation states. For example phosphonic acid ($H_3PO_3$)and phosphinic acid ($H_3PO_2$) have hydrogen atoms in two unique oxidation states where the hydrogen bound to oxygen is in the +1 oxidation state while the P-H bond is non-polar, and based on electronegativities would be assigned a -1 oxidation state. In both compounds the phosphorous atom is in the +5 oxidation state. Both compounds are reducing agents owning to the presence of the $P-H$ bonds and low oxidation state of $H$. 

```{raw} html
:file: ../../assets/ECHEM/phosphinic-onic.svg
```
```{figure} ../../assets/ECHEM/phosphinic-onic.svg
:height: 0px
:name: phosphinic

Lewis structures of phosphonic acid (left) and phosphinic acid (right)
```

In some cases, particularly with organic structures where electrons are delocalized across multiple atoms through resonance or $\pi$-conjugation it is perhaps more useful to assign a formal oxidation state to the structurally persistent molecule rather than individual atoms. Two example are shown below for the molecules quinone and napthalene both of which can be reduced by one electron to form the semiquinone and the napthalenide radical anions. 

```{raw} html
:file: ../../assets/ECHEM/organic-redox.svg
```
```{figure} ../../assets/ECHEM/organic-redox.svg
:height: 0px
:name: organic-redox

Organic structure of quinone (left) and napthalene (right) and their corresponding reduced forms semiquinone and napthalenide.
```