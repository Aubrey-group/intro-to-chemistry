# 1.7 | Stoichiometry of Electrochemical Reactions

**Word count:** <span id="word-count">0</span> words<br>**Reading Time:** <span id="read-time">0</span> minutes

Now that we know how to relate electrical measurements to familiar units of chemical reactions like moles and J/mol, we can use these relationships to easily monitor electrochemical reactions by observations of current (Amps) and voltage (Volts).

## Practice

1. Consider the electrochemical reaction of the zinc-copper cell: | Zn(s) | ZnSO<sub>4</sub> (*aq*) || CuSO<sub>4</sub> (*aq*) | Cu(s) |. By how much would the mass of the copper electrode increase if 1.0 A of current was passed through the cell for 1.0 hour? 

```{dropdown} Solution

First determine the number of moles of electrons transferred through the circuit. 

$1\,hour = 3600\,s$ and $1 A = 1 C/s$. Charge is the product of current and time, $Q = I \times t = 1\,A \times 3600\,s = 3600\,C$. 

Converting to moles of electrons using Faraday's constant, $F = 96485\,C/mol$, we find:

$$\text{moles}_{e^-} = \frac{3600\,C}{96485\,C/mol} = 0.0373\,mol_{e^{-1}}$$

The balance net reaction for the electrochemical cell is:

$$
Zn(s) + Cu^{2+} \rightarrow Zn^{2+} + Cu(s)
$$

But to relate moles electrons to moles of copper we need to consider how many electron are transferred. We can determine this by considering the *balanced* half-reactions:

$$
Zn(s) &\rightarrow Zn^{2+} + 2e^- \quad  \\ 
Cu^{2+} + 2e^- &\rightarrow Cu(s)
$$

**Note**: When balancing redox reactions *always* keep track of the number of electrons $n$ transferred in the balanced reaction. In this case $n = 2$. We see for the balance reactions that 2 moles of electrons are required for every 1 mole of copper produced.

$$
\text{moles}_{Cu} = \left(\frac{0.0373\,mol e^{-1}}{1}\right)\left(\frac{1\,mol\,Cu}{2\, mol e^{-1}}\right)
$$

Finally, the atomic mass of copper is 63.5463 g/mol. Converting moles of copper to grams we find **1.2 g of copper** would be deposited on the electrode after 1 hour of operation applying 1 A of current.

```

2. How long would it take to dissolve 100 g of zinc by applying 2.0 A of current?

```{dropdown} Solution

As in the previous example:

$$
Zn(s) &\rightarrow Zn^{2+} + 2e^- \quad  \\ 
Cu^{2+} + 2e^- &\rightarrow Cu(s) \\
\hline
Zn(s) + Cu^{2+} &\rightarrow Zn^{2+} + Cu(s)
$$

The atomic mass of zinc is 65.382 g/mol. Converting grams of zinc to moles: $100\,g_{Zn} = 1.529\,mol_{Zn}$.

The number of moles of electrons required to dissolve 1 mole of zinc is 2 ($n=2$ and there is 1 eq of Zn in the balance reaction). Therefore 3.059 moles of electrons are required to dissolve 100 g of zinc.

Using Faraday's constant to convert moles of electrons to Coulombs then dividing by the applied current of $1 A = 1 C/s$ to total time required to dissolve 100 g of zinc can be determined.

$$
t = \left(\frac{3.059\,mol_{e^-}}{1}\right)\left( \frac{96485\,C}{1\,mol_{e^-}}\right)\times \frac{1\,s}{2\,C} = \text{150,000 s    or    41 h} 
$$

```
