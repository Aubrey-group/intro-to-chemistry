# 1.12 | Applications of Electrolytic Cells

**Word count:** <span id="word-count">0</span> words<br>**Reading Time:** <span id="read-time">0</span> minutes


Electrolysis is the process of inputting electrical energy into an electrochemical cell to drive a non-spontaneous reaction, usually for the purpose of producing a higher value chemical product. 

## Water Splitting and Kinetic Overpotentials

The electrolysis of water to form hydrogen and oxygen gas, $2H_2O(l) \rightarrow 2H_2(g) + O_2(g)$, is an emerging technology for the conversion of electrical energy into fuel. While all the worlds most common carbon laden fuels necessarily emit carbon dioxide, the combustion of hydrogen gas yield only water vapor. Today use of hydrogen as a fuel is not wide spread but rather the majority of hydrogen produced today is used as a synthetic feedstock for the production of ammononia in the [Haber-Bosch process](https://en.wikipedia.org/wiki/Haber_process). What's more state-of-the-art hydrogen production is based upon the [steam methane reforming reaction](https://en.wikipedia.org/wiki/Steam_reforming), $CH_4(g) + H_2O(g) \rightarrow CO(g) + 3H_2(g)$ and the [water-gas shift](https://en.wikipedia.org/wiki/Water%E2%80%93gas_shift_reaction) reaction, $CO(g) + H_2O(g) \rightarrow CO_2(g) + H_2(g)$, ultimately emitting carbon dioxide as a byproduct. In the popular media hydrogen production that include carbon dioxide as a byproduct are often referred to as "grey hydrogen" while hydrogen produced from water splitting is referred to as "green hydrogen".

Because water is not conductive on its own it is not a good electrolyte without the addition of a dissolved salt. For water splitting any metal ion with a reduction potential lower than 0.0 V vs SHE is thermodynamically suitable and in practice alkali metal ions that are among the most difficult to reduce are often used. Chloride ions are also oxidized at a higher potential than at which water is oxidized and could potentially be used. Alternatively sodium hydroxide is a reasonable choice because while the hydroxide is consumed at the anode it will also be regenerated stoichiometrically at the cathode. Provide electrodes stabile to these oxidizing and basic conditions can be identified electrolysis at high pH could also be sustainable. 

<div style="max-width:300px;display:flex;margin:auto">

```{raw} html
:file: ../../assets/ECHEM/electrolysis-water.svg
```

</div>


```{figure} ../../assets/logo-dark.svg
:height: 0px
:name: water-splitting

cell diagram for the electrolysis of water to produce hydrogen and oxygen gas
```

### Kinetic Overpotential

While while thermodynamic potentials may inform use as to where or not a reaction is spontaneous (concentration of products is greater than reactants at equilibrium), thermodynamics makes not comment on how fast the reaction will proceed. In most cases a **kinetic overpotential** must be applied for an electrolytic reaction to proceed at a reasonable rate. In the case of water electrolysis we can see from the E-pH diagram (or table of standard electrode potentials) that a minimum thermodynamic potential of 1.23 V is required. In addition, a kinetic overpotential of $\eta \approx 1.2\,V$ is needed for the reaction to proceed at an economical rate. The *total* voltage applied accross the electrolytic cell is then $V_{\text{total}} = E_{cell} + \eta = 1.23\,V + 1.2\,V = 2.43\,V$.

In practice, with these high overpotentials if a NaCl electrolyte were chosen it would be possible to produce both oxygen gas and chlorine gas at the anode simultaneously. This may not be desireable as chlorine gas is toxic and corrosive and the oxidation of chloride to chlorine is irreversible requiring additional sodium chloride to constantly be added to the electrolyte and excess sodium hydroxide to be removed. Nonetheless it turns out this decomposition of sodium chloride electrolytes is central to perhaps the most important industrial electrolysis reaction, the chlor-alkali process.

## Chlor-Alkali Process

The chlor-alkali process is among the most important chemical reactions supporing modern civilization. Accounting about about 1% of annual global electricity consumption, then net reaction of this electrochemical cell produces three high value synthetic feedstocks: chlorine gas, hydrogen gas, and concentrated sodium hydroxide solutions. The chlor-alakli cell uses two electrolytes in the same sell. At the anode the anolyte is concentrated sodium chloride solution and at the cathode is a basic solution of sodium hydroxid. From the net reaction:

$$
2 NaCl(aq) + 2 H_2O(l) \rightarrow Cl_2(g) + H_2(g) + 2 NaOH(aq)
$$

Chloride ions are consumed at the anode where they are oxidized to chlorine gas effectively diluting the concentration of NaCl at the anode side of the cell. Additional sodium chloride is constantly added to maintain a stable concentration of NaCl electrolyte at the anode. At the cathode water is reduced to hydrogen gas (bubbles out the top of the cell) and hydroxide ions. At the cathode the concentration of NaOH is constantly increasing since hydroxide is a produce of the reduction half reaction. Continuously the NaOH solution at the cathode is removed and replaced with pure water to maintain a stable concentration of NaOH at the cathode.

```{raw} html
:file: ../../assets/ECHEM/Chlor-alkali.svg
```

```{figure} ../../assets/logo-dark.svg
:height: 0px
:name: chlor-alkali

Cell diagram for the chlor-alkali process where a sodium chloride is electrolyzed to produce the high value synthetic chemicals chlorine gas, hydrogen gas, and concentrated sodium hydroxide solution.
```

The two electrolytes used in the cell are *not* compatible. If chloride gas comes into contact with the sodium hydroxide solution they will react to from 

### Product Streams Originating from the Chlor-Alkali Process

Much of the usefulness of the products (H<sub>2</sub>, Cl<sub>2</sub>, and NaOH) formed by the chlor-alkali process comes from their implementation are reagents in *downstream* chemical reactions. 

**Chlorine and Hydrogen**, can react together to form hydrochloric acid. Chorine also reacts with sodium hydroxide to form bleach (aqueous NaClO). 

$$
H_2 + Cl_2 &\rightarrow 2 HCl \\
Cl_2 + NaOH &\rightarrow  NaCl + H_2O + NaClO \quad \text{(bleach)}\\
$$

One of the largest scale organic reactions with Cl<sub>2</sub> is the chlorination of ethylene (C<sub>2</sub>H<sub>4</sub>) to form dichloroethane. 

<div style="max-width:300px;display:flex;margin:auto">

```{raw} html
:file: ../../assets/ECHEM/dichloroethane.svg
```
</div>


The same dichloroethane product can also be formed by oxychlorination of ethylene using oxygen and HCl. 

<div style="max-width:300px;display:flex;margin:auto">

```{raw} html
:file: ../../assets/ECHEM/oxychlorination.svg
```
</div>

Dichlorethane subsequently is the primary feedstock for the production of polyvinyl chloride (PVC) plastics via an elimination reaction in base followed by a radical polymerization reaction. After polyethylene and polypropylene (direct products of fossil fuel), PVC is the third most produced organic polymer.

<div style="max-width:425px;display:flex;margin:auto">

```{raw} html
:file: ../../assets/ECHEM/PVC.svg
```
</div>

**Sodium Hydroxide** from the chlor-alkali process is also extensively used in a variety of direct applications including drilling mud, the neutralization of acidic waste water, the precipitation of toxic metals for water treatment, and production of soaps via saponification reactions.

Sodium hydroxide is also used as a reagent in a number of important industrial reactions:

1.  The *defulfurization of crude oil* where $H_2S + 2 NaOH \rightarrow Na_2S + H_2O$
2.  In the production of paper *pulp* where lignin is removed from cellulose: $\text{wood chips} + NaOH + H_2S \rightarrow \text{wood pulp}$
3.  The *Bayer Process* where aluminum oxide is separated from silicates and iron oxide in bauxite (mostely Al<sub>2</sub>O<sub>3</sub>) ore to afford pure aluminum oxide: 
 
$$
Al_2O_3 (\text{bauxite}) + NaOH &\rightarrow 2 NaAl(OH)_4 (aq) + CaSiO_4 (s) (\text{removed}) \\
NaAl(OH)_4 (aq) &\xrightarrow{\text{crystallization}} Al(OH)_3 (s)+ NaOH (aq)  \\
2 Al(OH)_3 (s) &\xrightarrow{\text{heat}} Al_2O_3 + 3 H_2O
$$

The purified aluminum oxide is then converted to aluminum metal by electrolysis at high temperature (see below).

## Industrial Production of Metals

Many of industrial metals, in particular those with very negative reduction potentials, are produced at large scale be electrolysis of their salts or oxide forms. Amoung the most common are sodium, magnesium, and aluminum.

The **Downs' Cell** is amoung the oldest electrolytic methods for the production of a metal. The the Downs' cell is contrstructed of two redox inert electrodes submerged in a molten sodium chloride salt bath at about 600 ºC: | Carbon | NaCl (l) | Steel cathode |. The chloride atoms are oxidized to form chlorine gas at the anode and sodium cations are reduced to sodium metal at the cathode. Other metal salts can be added to the sodium cloride metal to reduce the melting point and the improve the energy efficiency of the cell. The net reaction for the Downs' cell is:

$$
NaCl(l) \rightarrow Na(l) + 1/2 Cl_2(g)
$$

Not the be confused with the Downs' cell the **Dow Process** is an analogous electrochemical cell used to produce elemental Mg from a molten MgCl<sub>2</sub> salt bath. The net reaction for the Dow process is:

$$
MgCl_2(l) \rightarrow Mg(l) + Cl_2(g) \\
$$

The first electrolytic reduction to form elemental aluminum also employ the use of a molten choride salt bath. In the case of aluminum a deep eutectic mixtures of AlCl<sub>3</sub> and NaCl has a very low melting point of about 95 ºC. Since Al<sup>3+</sup> has a higher reduction potential than Na<sup>+</sup> only the aluminum ions in the salt bath are reduced to form aluminum metal. However, the most successful method the production of aluminum metal is the [Hall-Héroult process](https://en.wikipedia.org/wiki/Hall–Héroult_process). The net reaction for the Hall-Héroult process is:

$$
Al_2O_3(l) \rightarrow 2 Al(l) + 3/2 O_2(g)
$$

Because the oxide form of aluminum is naturally ocurring this method tend to be lower cost. However aluminum oxide, a refactory oxide, has an very high melting point (>2000 ºC) and thus the use of molten Al<sub>2</sub>O<sub>3</sub> as an electrolyte is not practical. Instead in the Hall-Héroult process,  aluminum oxide is dissolved in molten cryolite (Na<sub>3</sub>AlF<sub>6</sub>) with a lower melting point of 950 ºC. Here, the molten cryolite salt conducts ions and dissolves Al<sub>2</sub>O<sub>3</sub> but is otherwise redox inert. At the cathode Al<sup>3+</sup> is reduced to form aluminum metal and a sacrificial carbon anode oxidized to form CO<sub>2</sub> gas using dissolved Al<sub>2</sub>O<sub>3</sub> as the source of oxygen. Like the chlor-alkali process, the Hall-Héroult process is used globally at extremely large scale and estimated to consume about 1% of the worlds annual electricity production.

## Electrorefining

 Electrolysis can also be used to *purify metals* from mixture or alloy of metals via electrolysis of a crude mixture of metal ions in solution. Zinc has been produced by this method by first reaction Zinc ore (mainly ZnO) with H<sub>2</sub>SO<sub>4</sub> to form ZnSO<sub>4</sub>. Dissolving the precipitate in water, Zn(s) can be formed by the electrolytic reaction:

$$
2 ZnSO_4(aq) + 2 H_2O(l) \rightarrow 2 Zn(s) + 2 H_2SO_4(aq) + O_2(g)
$$

Copper is first smelted in the co-reduction of Cu<sub>2</sub>O and Cu<sub>2</sub>S to form Cu(*s*) and SO<sub>2</sub>. The copper is then dissolved in H<sub>2</sub>SO<sub>4> and the impurities are removed by electrolysis using crude copper as the anode and a pure copper cathode. Raw gold mined from earth deposits is also purified by similar electrolysis process using a chloroauric acid (HAuCl<sub>4</sub>) electrolyte solution to yield 24 carat gold.

Silver is also purified by via an electrorefinment process. As a common impurity in the electrorefinement of other metals like lead, copper, and zinc, silver can be recovered as byproducts of these other metal purification processes. The order in which each pure metal is extracted from a mixed metal anode will follow the electrochemical series with the metal ion with the most positive reduction potential being reduced first.

$$
Ag^+ + e^- &\rightarrow Ag(s) \quad E^{\circ} = 0.80\,V\\
Cu^{2+} + 2e^- &\rightarrow Cu(s) \quad E^{\circ} = 0.34\,V\\
Pb^{2+} + 2e^- &\rightarrow Pb(s) \quad E^{\circ} = -0.13\,V\\
Zn^{2+} + 2e^- &\rightarrow Zn(s) \quad E^{\circ} = -0.76\,V
$$

Thus is a crude lead ingot contains a substantial amount of silver as an impurity:
1. First the crude anode is oxidized to form silver ions in solution that are then reduced at the cathode to form pure silver metal.
2. Second, remaining lead ions are oxidized at the anode to form dissolve lead ions in solution that are then reduced at a pure lead cathode to form pure lead metal.

### Smelting

Smelting is the chemical reduction or metal oxides or metal salts to produce the pure metal. Metals that are have relatively high reduction potentials have been produced by smelting since antiquity. Iron, zinc, lead, titanium, and blister (crude) copper (see above) are all produced by smelting.

Iron is by far the largest scale smelting operation in the world. It is estimated that 1,300 million tons of pig iron (raw smelted iron) are produced annually.[^1]

<div style="max-width:300px;display:flex;margin:auto">

```{raw} html
:file: ../../assets/ECHEM/blast-furnace.svg
```

</div>

```{figure} ../../assets/logo-dark.svg
:height: 0px
:name: blast-furnace

Schematic diagram of a blast furnace for the smelting of iron ore to produce pig iron.
```

## Manufacture and Finishing of Metallic Forms

Electrolysis is also used in the more precise deposition and shaping of metals through a variety of manufacturing techniques:

1. **Electroplating** is the deposition of a thin layer of metal onto a surface. Similar to the case of electrorefining the metal that is to be deposited serves as the anode where it is oxidized and metal ions then conduct through the electrolyte to be reduced back to the metal on the work piece which serves as the cathode. Common examples of electroplating include the deposition of gold onto jewelry, the deposition of chromium onto automotive parts, and zinc onto steel to form galvanized steel. Electroplating is an additive process where material is added to the work piece.
2. **Electroforming** is a similar process to electroplating but is used to form a thicker free standing metal object where the cathode is a conductive mold of the desired shape. The deposited metal is then removed from the mold to yield a free standing object.
3. **Electrotyping** is a similar process to electroforming where the deposited metal make a precise reproduction of a mold (e.g. a woodcut) and is used in the printing industry to make printing plates. Electrotyping is an additive process where material is added to the work piece.
4. **Electropolishing** is instead a reductive process where metal material is removed from the rough surface of an object to yield a smoother finish. In an electropolishing cell the work pieces is the anode and oxidized from remove the most reactive metal (usually the sharpest edges) from the surface. Electroplishing is a subtractive process where material is removed from the work piece.
5. **Electrodischarge Machining (EDM)** is a process where a high voltage potential is applied between a work piece and a tool to remove material from the work piece. The tool is not in contact with the work piece but rather the high voltage potential causes a spark to jump between the tool and the work piece removing material from the work piece. This process is used to machine very hard materials like tungsten carbide and is used in the manufacture of precision parts for the aerospace industry. EDM is among the most precise methods of machining large parts with tolerances as tight as 2 µm. EDM is a subtractive process where material is removed from the work piece.
6. **Electrochemical Machining (ECM)** is a subtractive process like EDM ideal for use with very hard materials that cannot be machined by traditional methods of mechanical cutting or grinding. In ECM the anode is a metallic work piece that oxidized to dissolve metal ions in solution. The cathode takes a shape that is the negative of the desired shape of the work piece. As material is removed from the work piece the inert cathode is brought closer and closer to the work piece to maintain a constant gap between the two and ensure the desired shape is formed. 


## Practice




## References

[^1]: U.S. Geological Survey, Mineral Commodity Summaries, January 2024