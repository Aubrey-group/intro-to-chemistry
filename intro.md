# Fundamentals of Chemistry

CH302C is the second half of the Foundations of Chemistry designed to provide you with a fundamental understanding of the principles and concepts of chemistry. The course covers major themes including electrochemistry, reaction kinetics, and the descriptive chemistry of the elements.

**UNIT 1 ECHEM** | In the electrochemistry unit, you will learn about how charged particles in solutions and molten salts interact with electric currents and undergo chemical reactions. We will explore the principles, construction, and mechanics of electrochemical cells and how they can be used to generate electricity, store energy, and produce useful chemicals.

**UNIT 2 KINETICS** | In the reaction kinetics unit you will gain an atomistic understanding of the rates of chemical reactions and the underlying mechanisms factors that influence them. You will learn how microscopic mechanisms of chemical reactions can be derive by carful analysis of reaction rates, the role catalysts and enzymes in reaction mechanisms, and the methods used to measure reaction rates.

**UNIT 3 MOLECULAR CHEMISTRY** | In the molecular chemistry unit you learn how the particular properties of the elements influence the structure and chemical properties more complex chemical compounds. You will learn the nomenclature for specifying more complex organic and inorganic structures, how to identify functional groups, and how through the identification common structural motifs chemical properties may be predicted. In doing so we will survey many of the most important chemicals and materials that define modern civilization.

**GROUP PROJECT** | For the group project, you will work in teams to research and analyze the most important advances in chemistry. The properties of chemicals, materials, and their fundamental transformations will be the core focus of project. Each team will choose a specific topic and produce and develop project content that leverages the totality of the tools of chemical analysis you have learned in CH301 & CH302.  The purpose of this project is to expand your chemical intuition and knowledge beyond the assigned reading and lecture materials. You and your team will conduct research, learn to read the primary scientific literature, analyze data. You will present your findings as both a multimedia report (prose, illustration, and video) and an oral presentation to the class. This project will provide you with an opportunity to apply to develop your research and scientific communication skills as well as demonstrate your understanding of fundamental chemistry outside the traditional modes direct examination.


## Contents 

1. Electrochemistry
2. Reaction Kinetics
3. Molecular Chemistry
 