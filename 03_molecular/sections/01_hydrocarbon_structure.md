# 3.1 | The structures of alkanes

**Word count:** <span id="word-count">0</span> words<br>**Reading Time:** <span id="read-time">0</span> minutes

The systematic naming of chemical compounds started to become a necessity with the advent of chemical analysis and our understanding of how chemical composition related to chemical properties. Many of the important conventional names we have for the inorganic and common organic compounds were originally codified by Morveau, Lavoisier, Bertholet, and Fourcroy in *Méthode de nomenclature chimique* in 1787.[^1] However it wasn't until the mid 19th century that our isolation and understanding of the linear hydrocarbons had by developed to the point that their conventional could be established. Carl Reichenbach and August Wilhelm von Hofmann ultimately established the nomenclature of the linear alkanes by the mid 1860s. 

## Alkanes

Simple linear and branched alkanes are named based on the length of the longest chain of carbon. In order to identify more complex structure the names of the primitive linear alkanes must first be established, {numref}`alkanes`.


```{raw} html
:file: ../../assets/MOL/alkanes.svg
```
```{figure} ../../assets/MOL/alkanes.svg
:height: 0px
:name: alkanes

Names of the first 8 linear alkanes and their corresponding group names
```

### Branches 

The group are used for naming branches off of the longest hydrocarbon chain. To indicate which position the branch is located on, the carbonds on longest chain are numbered starting at the end closest to a branch. The branch is then named based on the number of carbons in the branch and its position on the longest chain. A few examples are given in {numref}`alkane_branches`. 



```{raw} html
:file: ../../assets/MOL/alkane-branches.svg
```
```{figure} ../../assets/MOL/alkane-branches.svg
:height: 0px
:name: alkane_branches

Naming conventions for branched longest hydrocarbon chain
```

Branches on branches attached to the longest are also possible. To name these first name the branch as if it were its own alkane and then convert it to a group name ending in -yl. The position of the branch is then indicated by the number of the carbon on the longest chain to which the branch is attached.


### Cyclic Alkanes

There are also a number of common cyclic hydrocarbons that have very similar naming conventions to the linear alkanes, {numref}`cycloalkanes`. Cyclic alkanes will form the root of the name of branched structures if the ring equal to or longer than any linear carbon chain. 



```{raw} html
:file: ../../assets/MOL/cycloalkanes.svg
```



```{figure} ../../assets/MOL/cycloalkanes.svg
:height: 0px
:name: cycloalkanes

Names of the cycloalkanes
```

## 01 | Practice: Naming hydrocarbons

1. Name the following hydrocarbon structures. 



<div id="flashcard" class="flashcard" data-file="../../_static/hydrocarbon_flashcards.json" style="display:grid; grid-template-rows: 1fr auto; min-height:400px; margin:auto">
    <div id="content" class="content" style="display:flex;margin:auto;"></div>

<div style="display:flex;justify-content:center; align-items:center;">
<button id="prev" class="prev">Previous</button>
<button id="flip" class="flip">Flip</button>
<button id="next" class="next">Next</button>
</div>
</div>


2. Draw the following hydrocarbon structures given the name. 



<div id="flashcard2" class="flashcard" data-file="../../_static/hydrocarbon_struct_flash.json" style="display:grid; grid-template-rows: 1fr auto; min-height:400px; margin:auto">
    <div id="content" class="content" style="display:flex;margin:auto;"></div>

<div style="display:flex;justify-content:center; align-items:center;">
<button id="prev" class="prev">Previous</button>
<button id="flip" class="flip">Flip</button>
<button id="next" class="next">Next</button>
</div>
</div>



## Further Reading


1.  Nickon, A.; Silversmith, E. F. [*Organic Chemistry: The Name Game*](https://books.google.com/books/about/Organic_Chemistry_The_Name_Game.html?id=1jb9BAAAQBAJ); *Elsevier*, **1987**.


## References

[^1]: Morveau, L.-B. G.; Lavoisier, A. L.; Berthollet, C.-L.; Fourcroy, A.-F. [*Méthode de nomenclature chimique*](https://www.loc.gov/item/15011439/). Library of Congress, Washington, D.C. 20540 USA. https://www.loc.gov/item/15011439/ (accessed 2024-04-19).


