# 3.2 | Organic functional groups


## Practice

1. Name the following structures. 



<div id="flashcard" class="flashcard" data-file="../../_static/functional-groups-name.json" style="display:grid; grid-template-rows: 1fr auto; min-height:400px; margin:auto">
    <div id="content" class="content" style="display:flex;margin:auto;"></div>

<div style="display:flex;justify-content:center; align-items:center;">
<button id="prev" class="prev">Previous</button>
<button id="flip" class="flip">Flip</button>
<button id="next" class="next">Next</button>
</div>
</div>

2. Draw the following structures.
 
<div id="flashcard" class="flashcard" data-file="../../_static/functional-groups-structure.json" style="display:grid; grid-template-rows: 1fr auto; min-height:400px; margin:auto">
    <div id="content" class="content" style="display:flex;margin:auto;flex-direction: column;text-align:center"></div>

<div style="display:flex;justify-content:center; align-items:center;">
<button id="prev" class="prev">Previous</button>
<button id="flip" class="flip">Flip</button>
<button id="next" class="next">Next</button>
</div>
</div>