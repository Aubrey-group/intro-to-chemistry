<script type="module" src="https://unpkg.com/@google/model-viewer/dist/model-viewer.min.js"></script>

# 3.3 | Coordination Chemistry

**Word count:** <span id="word-count">0</span> words<br>**Reading Time:** <span id="read-time">0</span> minutes

## Metal cations in solution

Thus far most of the time when we've encountered a reaction that included a the dissolution of a metal salt:

$$
FeSO_4 (s) \xrightarrow{H_2O (l)} Fe^{2+} (aq) + SO_4^{-2} (aq)
$$

or an acid-base reaction with a metal cation:

$$
Mg^{+2} + (aq) + H_2O (l) \rightleftharpoons Mg(OH)_2 (aq) + 2 H^+ (aq)
$$

or the anodic corrosion of a metal:

$$
Zn (s) \xrightarrow{H_2O (l)} Zn^{2+} (aq) + 2e^-
$$

we simple described the state of the metal cation as "aqueous". But what exactly does this mean? What does the structure of water and the metal cation actually looks like? Is is just a few ion diffusing through an ocean of water molecules?

The short answer is no. It turns our these metal cations will almost always form structurally stable molecules with water with well defined stoichiometric and geometries just like that of other organic and molecular structures we've already encountered in our discussions of the p-block compounds. For example, the dissolution of $FeSO_4 (s)$ in water actually forms the molecular ion $Fe(H_2O)_6^{2+}$, {numref}`FeH2O6` and a dissociated $SO_4^{2-}$ anion that participates in hydrogen bonding with other water molecules nearby. 

<model-viewer src="../../_static/models/Fe-H2O-6.gltf" ar ar-modes="webxr scene-viewer quick-look" camera-controls  shadow-intensity="0.0" tone-mapping="commerce" interaction-prompt="none" auto-rotate
style="height:200px !important; width:200px !important;display:flex !important; margin:auto !important">
</model-viewer>

```{figure} ../../logo.svg
:height: 0px
:name: FeH2O6

Structure of Fe<sup>2+</sup> in water has six water molecules *coordinated* to it to form the molecular ion Fe(H<sub>2</sub>O)<sub>6</sub><sup>2+</sup>
```

Here we'll focus on the bonding at the metal cations. Molecular ions like these possess a great wealth of useful properties including pigmentation, magnetism, redox activity, and catalytic activity, and play important roles in many biological, mineralogical, and industrial processes. 

## Coordination (Dative) Bonds

Acids and bases can be thought of as a pair of atoms or molecules where one electron rich species donates a pair of electrons (the Lewis base) to an electron poor species (the Lewis acid). Lewis' definition[^1] of acids and bases is perhaps the better way to think about acid-base reactions for three reasons (1) It allows us to explain why the pH of a solution decreases when we add a metal cation like Mg<sup>2+</sup> in MgCl<sub>2</sub> to solution even though it does not have any protons to donate as expected for an acid within the atom focused context of Arrhenius, Brønsted and Lowry's[^2] theories of acids, and (2) Lewis-Acid base theory focuses on the exchange and dynamics of the electrons which being much lighter than atoms move and adapt to changes in their environment many orders of magnitude faster than atomic nuclei. That is Lewis acid-base theory aligns well with the Born-Oppenheimer approximation[^3] of molecular dynamics. (3) easily expanded to account for interactions that do not involve protons or solvent molecules at all

A **coodination bond**, also called a **dative bond**, is a stable chemical bond between a Lewis base (electron donor) and a Lewis acid. In the case of $Fe(H_2O)_6^{2+}$ the water molecules are the Lewis bases and the $Fe^{2+}$ cation is the Lewis acid. In a *covalent bond* electrons are shared somewhat equitably between the atom pair; electrons are localized between atoms. In an *ionic bond* two charged atoms are attracted to each other by electrostatics and there is very little co-location of any electrons between the anion and the cation; electrons are localized at atoms. For any covalent bond between two different elements there exists some polarity owing to their difference in electronegativity and in more extreme cases bonds like C-F, H-F and H-O are considered to be *polar covalent*. 

Now consider the interaction between Fe<sup>2+</sup> and H<sub>2</sub>O the interact by water donating one of it's lone pairs of electrons to the iron center $H_2\ddot{O}\!\!:\rightarrow Fe^{2+}$. Considering the distribution of electrons, like an ionic bond the electrons "belong" to the oxygen but are to some extent donated to screen the positive charge at the iron center. In this respect on a spectrum of bonding, the coordination bond could be placed between a polar covalent bond and an ionic bond in terms of polarity. However, $H_2\ddot{O}\!\!:\rightarrow Fe^{2+}$ is certainly not an ionic bond; an ionic bond require both a positive and and a negative charge and water is of course charge neutral. Thus the coordination bond is a unique type of bonding only found in certain interactions between a Lewis acid and a Lewis base.

Above the coordination bond is represented as a single arrow $H_2\ddot{O}\!\!:\rightarrow Fe^{2+}$ to distinguish the relationship from a covalent bond as in $H–F$ represented as a solid line. This is to indicate that the electron pair "belongs" to the water molecular but in part donates some of it's electron density to the iron center. 

### Coordination bonds are relatively weak

Coordination bonds are stable at room temperature but generally weaker than many of the common covalent interactions we are familiar with like those found in organic molecules and polyatomic ions like sulfate ($SO_4^{2-}$) and phosphate ($PO_4^{3-}$). In many cases the coordination bonds can be reversibly broken and reformed at relatively low temperatures a key feature needed for many catalytic processes. Indeed these weaker bonding interactions that metals participate is a key reason why so many catalysts and enzymes contain a metal center as the active site. 

Because the bonding interaction between the metal cation and the Lewis Base is relatively weak, even with the $Fe(H_2O)_6^{2+}$ molecular ion, the iron cation and the water molecules retain much of their individual character. Generically we refer to the iron ion as the *central atom** or **metal center** and the water molecules as the **ligands**. Ligands are just Lewis bases that are coordinated to a metal center. The number of ligands coordinated to a metal center is called the **coordination number** ($CN$). In $Fe(H_2O)_6^{2+}$, $CN=6$.

[^1]: Lewis, G. N. [Valence and the Structure of Atoms and Molecules](https://archive.org/details/valence-and-the-structure-of-atoms-and-molecules-lewis-gilbert-newton-new-york-1923/page/35/mode/2up); The Chemical Catalogue Company: New York, 1923.
[^2]: (a) Svante Arrhenius. [Recherches sur la conductibilité galvanique des électrolytes](https://archive.org/details/recherchessurla00arrhgoog/page/n42/mode/2up); P.A. Norstedt & Söner, 1884. (b) Lowry, T. M. [The Uniqueness of Hydrogen](https://doi.org/10.1002/jctb.5000420302). J. Chem. Technol. Biotechnol. 1923, 42 (3), 43–47.
[^3]: Born, M.; Oppenheimer, R. "[Zur Quantentheorie Der Molekeln. Annalen der Physik](https://doi.org/10.1002/andp.19273892002)" **1927**, *389* (20), 457–484.



## Chelate Ligands

Larger more richly structured ligands may have more than one atom in their structure that can act as a Lewis base and donate a pair of electrons to a metal center. Acetylacetonate is one such example {numref}`Fe-acac-H2O4` which can donate two pairs of electrons (1 pair per oxygen atom). Ligands like these are called **chelate ligands** and the number of atoms in the ligand that coordinate to the metal center is called the **denticity**. In the case of acetylacetonate, the denticity is 2. The naming convention is to indicate the denticity with a Greek letter $\kappa$ followed by the number of atoms that coordinate to the metal center. For acetylacetonate the ligand is $\kappa^2$-acetylacetonate.


<model-viewer src="../../_static/models/Fe-acac-H2O4.gltf" ar ar-modes="webxr scene-viewer quick-look" camera-controls  shadow-intensity="0.0" tone-mapping="commerce" interaction-prompt="none" auto-rotate
style="height:400px !important; width:400px !important;display:flex !important; margin:auto !important">
</model-viewer>

```{figure} ../../logo.svg
:height: 0px
:name: Fe-acac-H2O4

Structure of Fe(H<sub>2</sub>O)<sub>4</sub>($\kappa^2$-acac) (acac<sup>–1</sup> = acetylacetonate)
```

In certain cases the atoms that coordinate to the metal center are also directly bonded to each other. In this case the ligand is described as having hapticity instead of denticity, {numref}`hapticity`.


```{raw} html
:file: ../../assets/MOL/hapticity.svg
```

<div style="display: flex; justify-content: space-around;max-width:600px; margin:auto">
<model-viewer src="../../_static/models/ferrocene.gltf" ar ar-modes="webxr scene-viewer quick-look" camera-controls  shadow-intensity="0.0" tone-mapping="commerce" interaction-prompt="none" auto-rotate
style="height:100px; width:100px;display:flex; margin:auto">
</model-viewer>
<model-viewer src="../../_static/models/Ni-allyl.gltf" ar ar-modes="webxr scene-viewer quick-look" camera-controls  shadow-intensity="0.0" tone-mapping="commerce" interaction-prompt="none" auto-rotate
style="height:100px; width:100px;display:flex; margin:auto">
</model-viewer>
<model-viewer src="../../_static/models/Co-cp-ethylene-H.gltf" ar ar-modes="webxr scene-viewer quick-look" camera-controls  shadow-intensity="0.0" tone-mapping="commerce" interaction-prompt="none" auto-rotate
style="height:100px; width:100px;display:flex; margin:auto">
</model-viewer>
<model-viewer src="../../_static/models/Cr-benzene-2.gltf" ar ar-modes="webxr scene-viewer quick-look" camera-controls  shadow-intensity="0.0" tone-mapping="commerce" interaction-prompt="none" auto-rotate
style="height:100px; width:100px;display:flex; margin:auto">
</model-viewer>
</div>



```{figure} ../../logo.svg
:height: 0px
:name: hapticity

Structure of common hapto ligands: cyclopentadienyl (Cp), allyl, ethylene; and benzene, as well as their corresponding metal complexes: ferrocene, nickel bis-($\eta^3$-allyl), Co($\eta^2$-ethyl)($\eta^5$-cyclopentadienyl)(ethyl), and chromium bis-($\eta^6$-benzene).
```

## Practice

For each of the following structures identify the denticity, number of lone pairs the ligand can donate to a metal center, and the common name of the ligand.

<div id="flashcard" class="flashcard" data-file="../../_static/ligands-id.json" style="display:grid; grid-template-rows: 1fr auto; min-height:400px; margin:auto">
    <div id="content" class="content" style="display:flex;margin:auto;"></div>

<div style="display:flex;justify-content:center; align-items:center;">
<button id="prev" class="prev">Previous</button>
<button id="flip" class="flip">Flip</button>
<button id="next" class="next">Next</button>
</div>
</div>


## References
