# 3.4 | Hard-Soft Acid-Base Theory









## Practice

1. Predict whether each of the following compounds will be either a hard acid, soft acid, hard base, or soft base.

<div id="flashcard" class="flashcard" data-file="../../_static/HSAB-id.json" style="display:grid; grid-template-rows: 1fr auto; min-height:200px; margin:auto">
    <div id="content" class="content" style="display:flex;margin:auto;"></div>

<div style="display:flex;justify-content:center; align-items:center;">
<button id="prev" class="prev">Previous</button>
<button id="flip" class="flip">Flip</button>
<button id="next" class="next">Next</button>
</div>
</div>

2. For each of the following predict which acid-base interaction will be stronger.

<div id="flashcard2" class="flashcard" data-file="../../_static/HSAB-strength.json" style="display:grid; grid-template-rows: 1fr auto; min-height:250px; margin:auto">
    <div id="content" class="content" style="display:flex;margin:auto;"></div>

<div style="display:flex;justify-content:center; align-items:center;">
<button id="prev" class="prev">Previous</button>
<button id="flip" class="flip">Flip</button>
<button id="next" class="next">Next</button>
</div>
</div>

