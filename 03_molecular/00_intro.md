# 3 | Molecular chemistry

## Sources


## Slides

### Deck 1 | Hydrocarbons

```{image} ../assets/MOL/D1-hydrocarbons.png
:alt: D1-hydrocarbons.png
:width: 200px
```
Slides ([pdf](../assets/MOL/MOL-D1-hydrocarbons.pdf))

### Deck 2 | Functional Groups

```{image} ../assets/MOL/D2-functional-groups.png
:alt: D2-functional-groups.png
:width: 200px
```
Slides ([pdf](../assets/MOL/MOL-D2-functional-groups.pdf))


### Deck 3 | Coordination Chemistry

```{image} ../assets/MOL/D3-INORG.png
:alt: D3-INORG.png
:width: 200px
```
Slides ([pdf](../assets/MOL/MOL-D3-INORG.pdf))

### Deck 4 | HSAB Theory

```{image} ../assets/MOL/D4-HSAB.png
:alt: D4-HSAB.png
:width: 200px
```
Slides ([pdf](../assets/MOL/MOL-D4-HSAB.pdf))

### Deck 5 | Crystal Field Theory

```{image} ../assets/MOL/D5-CFT.png
:alt: D5-CFT.png
:width: 200px
```
Slides ([pdf](../assets/MOL/MOL-D5-CFT.pdf))

### Deck 6 | Ligand Field Theory

```{image} ../assets/MOL/D6-LFT.png
:alt: D6-LFT.png
:width: 200px
```
Slides ([pdf](../assets/MOL/MOL-D6-LFT.pdf))

### Deck 7 | Tetrahedral Ligand Fields

```{image} ../assets/MOL/D7-LFSE.png
:alt: D7-LFSE.png
:width: 200px
```
Slides ([pdf](../assets/MOL/MOL-D7-LFSE.pdf))

### Deck 8 | Absorption Spectroscopy

```{image} ../assets/MOL/D8-spectroscopy.png
:alt: D8-spectroscopy.png
:width: 200px
```
Slides ([pdf](../assets/MOL/MOL-D8-spectroscopy.pdf))


## Learning Objectives


### Organic Chemistry

- Drawing and naming hydrocarbons
- Draw and understand the equivalence of lewis structures, line angle structures with hydrogens, and line angle structures with implicit hydrogens
- Know the naming conventions of the simple linear alkanes (ethane, propane, butane, ...) and the group names (ethyl, propyl, butyl, ...)
- Know the basic naming conventions of alkanes with branches
- Find and number the longest chain in a simple hydrocarbon
  identify the group names for the branches and their carbon index along the longest carbon chain
- Know the basic naming conventions of alkenes and alkynes
- Know the structure of benzene and simple cyclic alkanes (cyclopropane, cyclopentane, cyclohexane, ...)
- Be able to number cycloalkanes and not the position of branched groups (e.g. 1,2-dimethyl-cyclohexane)
- For alkenes be able to draw both the cis and trans isomers when possible and understand when there is not difference between cis and trans.
- Be able to label the degree of a carbon (primary (1º), secondary (2º), tertiary (3º), quaternary (4º))

#### Isomers

- Isomers are 2 or more molecules with the same chemical composition (stoichiometry) where the atoms are arranged differently with respect to each other.
- Be able to identify if compounds are:
  - structural isomers (same composition, difference connectivity)
  - geometric isomers (same connectivity, difference arrangement in space (e.g. cis vs trans))
  - optical isomers (enantiomers)
  - completely different compounds (different chemical compositions)

#### Organic Reactions

- Be able to identify a substitution reaction
- Be able to determine the products of a substitution reaction given the reactants and the type of reaction
- Be able to identify an elimination reaction
- Be able to determine the products of a elimination reaction given the reactants and the type of reaction
- Be able to identify an electrophilic addition reaction
- Be able to determine the products of a electrophilic addition reaction given the reactants and the type of reaction
  Dehydrohalogenation = a specific type of elimination reaction
  hydrohalogenation = a specific type of electrophilic addition reaction reaction
- Be able to identify an electrophilic substitution reaction at a benzene ring
- Be able to determine the products of a electrophilic substitution reaction given the reactants and the type of reaction
- Be able to identify a condensation reaction (e.g. formation of an ester/amide bond)
- Be able to determine the products of a condensation reaction reaction given the reactants and the type of reaction
  Resonance

#### Resonance

- Be able to draw the resonance structures give a molecules structures
- Determine if the resonance structure you drew are equienergetic (chemically equilvalent) or not equienergetic.
- Understand how resonance can make certain functional groups "electron withdrawing" (i.e nitrobenenze) or "electron donating" (i.e. phenol) and how that may influence the products or rate of a reaction.

#### Conjugation and aromaticity

- Identify and count the number of $\pi$ bond that are "conjugated" in a molecule
- Understand the approximate relationship between the extent of conjugation and the color of a hydrocarbon.
- Determine if a molecule is aromatic (cyclic, $4n+2$ $\pi$ electrons), anti aromatic (cyclic, $4n$ $\pi$ electrons), or neither (either non cyclic or the $\pi$ electron count is not equal to $4n$ or $4n+2$) where $n = 0, 1, 2, 3, 4, ...$

#### Functional groups

- Be able to identify and name compounds with the following functional groups
  fluoro, chloro, bromo, iodo, hydroxy, ether, phenol, aldehyde, ketone, carboxylic acid, ester, amine, amide, alkene, alkyne, phenyl
- Draw dipoles on functional groups and locate atoms with partial positive or partial negative charges
- Identify nucleophilic and electrophilic regions of molecules
- Understand the basic reactions of the different functional groups
  - haloalkanes: nucleophilic substitution
  - alcohols: acid base chemistry, oxidation to aldehyde/ketones, nucleophilic substitution with haloalkanes to form ethers, condensation with carboxylic acid to form esters
  - Synthesis of ethers from alcohols and haloalkanes
  - Synthesis of aldehydes via oxidation of primary alcohols
  - Synthesis of ketone via oxidation of secondary alcohols
  - Synthesis of esters from carboxylic acid and alcohol
  - Synthesis of peptides from condensation of amino acids
- Some basic properties of each functional group
  - **Haloalkanes** are polar covalent bonds and good "building block" molecules
  - **Alcohols** are usually very weak acids (weaker than water) and participate in hydrogen bonding. Their conjugate bases (alkoxides) are excellent nucleophiles (make ethers and esters)
  - **Ethers** are volatile and the key functional group of the industrially important polyethylene glycol
  - **Phenols** are more acidic that other alcohols due to resonance structures and the delocalization of electrons within the benzene ring. Used to make important resins, flavor molecules and pigments.
  - **Aldehydes** and ketones are oxidized alcohols, the have a strong dipole (polar) along the C=O bond, and the carbon is trigonal planar. They are often found in flavors and fragrance compounds
  - **Carboxylic acids** are weak acids (pKa's often around ~4-5). Acids taste sour and are often pungent (e.g. oxidized fats = rotten). Many are described as onerous, foul, sometimes sharp (like vinegar)
  - **Esters** are also often found within flavors and fragrances. They are formed by condensation reactions of carboxylic acids and alcohols (these compounds are often sweet or fruity)
  - **Amines** are weak bases (pKa ~10). The also smell sharp or often fishy.
  - **Amino acids** are not really functional groups so much as building block molecules of proteins that contain both an amine and a carboxylic acid group
  - **Amides** are like esters but formed with an amine instead of an alcohol. While the flavor chemistry of umami is more complicated than others typical flavor molecules often include amide bonds.

### Coordination Chemistry

#### Ligands and the Dative Bond

- Communicate the difference between a dative bonding interaction and other types of bonding like covalent, ionic, and hydrogen bonding.
- Fundamentals or lewis acid and lewis base chemistry and thinking or very polar covalent bonds as a pair of electron donor and electron acceptors
- Know the names of common ligands, their charge, and the number of electron pairs that donate to a metal center
- Given the structure of a ligand predict how electron pairs it can donate to a metal center and draw the chemical structure of the ligand coordinated to the metal
- Explain why chelate structures are more stable the similar structures without a chelate ligand
- Determine the number of d electrons a metal center has based on its formal oxidation state
- Determine the total number of electrons for a metal center based on the number of d-electrons and the number of electrons donated to the metal center by ligands
- Identify geometric, structural, and optical isomers within tetrahedral and octahedral metal complexes.
- Be able to draw different isomers of a metal complex given at starting compounds structure

#### HSAB Theory

- Determine if a given ligand is going to be a hard or soft Lewis Base
- Determine if a given metal ion is going to act as a hard or soft Lewis Acid
- Given two pairs of coordinated Lewis acid-base pairs, predicted which one will have the strongest interaction based on possible Hard-Hard, Soft-Soft, and Hard-Soft interactions
- Sort acids and bases on a spectrum from hard to soft

#### Crystal Field Theory

- Draw and label the 5 d-orbitals in their correct orientations
- Communicate how the d-orbitals' energies change going from a free ion to a spherical crystal field and then after distorting the spherical field to a lower octahedral symmetry
- Draw the orbital diagram for the d-obital splitting in an octahedral field
- determine the number of d electrons in a complex give the molecular formula
- Correctly fill the d-orbitals with electrons following the Aufbau Principle and Hunds rules for BOTH the High Spin (HS) scenario and the Low Spin (LS) scenario
- Calculate the CFSE for an octahedra complex
- Determine the total spin (S) for the molecular by adding the component ms values of the d-electrons
- Predict based on your orbital splitting diagram if a compound will be diamagnetic or paramagnetic
- Use the spectrochemical series to qualitatively predict $\Delta_{oct}$ and predict if you may expect a metal center to be high spin (HS) or low spin (LS)
- Know the d-orbital splitting pattern (2,3) for tetrahedral and that $\Delta_{tet}$ is always smaller than $\Delta_{oct}$ ($\Delta_{tet}= \frac{4}{9} \Delta_{oct}$)
- Tetrahedral metal complexes will always be High Spin for the 1st row transition metals
- 2nd and 3rd row transition metals are always Low Spin regards of the shape of the ligand field.

#### Ligand Field Theory

- Communicate the origins of ligand $\pi$ donation (pi base) and why it decreases $\Delta_{oct}$
  Predict if a ligand structure will participate in ligand $\pi$ donation (i.e. look for additional lone pairs (more the one) on the donating ligand atom)
- Communicate the origins of ligand $\pi$ acceptor (pi acid) interactions and why it increases $\Delta_{oct}$
- Predict if a metal center, based on it's d-electron configuration, can participate in $\pi$ backbonding (bonding interaction with ligand $\pi$-acceptor)
- Predict if a ligand (given its structure) will participate in ligand $\pi$ acceptor interactions with a metal center (i.e. look for double bond in the ligand near the metal center)
- Draw a sigma bonding interaction (cartoons of the orbital shapes) between a ligand p-orbital and either a $d_{z^2}$  or $d_{x^2-y^2}$ 
- Draw a $\pi$ bonding interaction (cartoons of the orbital shapes) between a ligand p-orbital and one of the dpi orbitals: $d_{xz}$ , $d_{yz}$ , $d_{xy}$ 
- Do so for a ligand $\pi$ donor
- Do so for a ligand $\pi$ acceptor (metal $\pi$ backbonding)

#### Absorption Spectroscopy

- Use the different units and measure of monochromatic light (wavelength, photon energy, frequency, wavenumber) (nm, eV, cm<sup>–1</sup>)
- Communicate how we observe color and how molecules interact with light reflected off a surface to display coloration
- Communicate how we observe color and how molecules interact with light transmitted through a clear solid or solution (Beer's Law)
- Colors get darker as the concentration of absorbing molecules in solution increases and the path light travels through solution increases
- For metal complexes explain and predict how a spectrum will change with an increase and decrease in d-orbital splitting
- As above you should be able to predict if a $\Delta_{oct}$ will increase or decrease if you change the metal ion's charge, the metal ions identical, or the ligand identity (i.e. spectrochemical series)
- Given a UV-vis absorption spectrum predict the color of a compound
- Communicate how all compounds tend to absorb light in the ultraviolet range of the electromagnetic spectrum
