# 2 | Reaction Kinetics

## Sources

1. Pauling, L. *General Chemistry*, 3<sup>rd</sup> ed.; W. H. Freeman: San Francisco, **1970**.
2. Atkins, P. W.; Jones, L. Chemical Principles: The Quest for Insight, 2<sup>nd</sup> ed.; Freeman: New York, **2002**.
3. Atkins, P. W.; Beran, J. A. *General Chemistry*, 2<sup>nd</sup> ed.; Scientific American Books : W.H. Freeman: New York, **1992**.
4. Housecroft, C. E.; Sharpe, A. G. *Inorganic Chemistry*, 4<sup>th</sup> ed.; Pearson: Harlow, **2012**.
5. Atwood, J. D. *Inorganic and Organometallic Reaction Mechanisms* VCH Publishers, New York **1997**.

## Lecture Slides

### Deck 1 | Measuring the rate of a reaction

```{image} ../assets/KINETICS/D1-Rates.png
:alt: D1-Rates.png
:width: 200px
```
Slides ([pdf](../assets/KINETICS/Kinetics-D1-Rates.pdf) )

### Deck 2 | Integrated rate laws

```{image} ../assets/KINETICS/D2-Laws.png
:alt: D2-Laws.png
:width: 200px
```
Slides ([pdf](../assets/KINETICS/Kinetics-D2-Laws.pdf) )


### Deck 3 | Radioactivity and multistep reactions

```{image} ../assets/KINETICS/D3-SSA.png
:alt: D3-SSA.png
:width: 200px
```
Slides ([pdf](../assets/KINETICS/Kinetics-D3-SSA.pdf))

### Deck 4 | Arrhenius and collision theory

```{image} ../assets/KINETICS/D4-TST.png
:alt: D4-TST.png
:width: 200px
```
Slides ([pdf](../assets/KINETICS/Kinetics-D4-TST.pdf) )


### Deck 5 | Catalysis

```{image} ../assets/KINETICS/D5-Catalysis.png
:alt: D5-Catalysis.png
:width: 200px
```
Slides ([pdf](../assets/KINETICS/Kinetics-D5-Catalysis.pdf) )



### Deck 6 | Review

```{image} ../assets/KINETICS/D6-Review.png
:alt: D6-Review.png
:width: 200px
```
Slides ([pdf](../assets/KINETICS/Kinetics-D6-Review.pdf) )


## Learning Objectives

1. Measuring rates of reaction: average rate, initial rate, instantaneous rate, macroscopic (change in concentration / change in time), microscopic (k, 1. molecular probabilities, molecularity)
2. Determine a rate from the slope of a concentration vs time plot
3. 4 factors that effect rate (concentration, medium, temperature, & catalysis)
4. Engineering considerations that impact reaction rates (thermal transport, mass transport)
5. Determine experimental rate laws from a table of experimentally determined initial rates
6. Understand the order of a reaction and it's impact on reaction rates
7. Apply integrated rate laws to solve for unknowns in  0<sup>th</sup>, 1<sup>st</sup>, and 2<sup>nd</sup> order reactions
8. Use the half lives to calculate a rate constant and predict how low it will take for a fraction or reactant to react or determine how much or a sample has decomposed in a given amount of time. 
9. Basics of Radioactivity (first order kinetics, most common routes of spontaneous decay: alpha, beta, electron capture, positron emission, and spontaneous fission, ....)
10. Mechanism, elementary reactions, molecularity
11. Identify a reactive intermediate
12. Write the overall net reaction given a reaction mechanism
13. Determine the rate determining step of a reaction mechanism.
14. Calculate the rate laws for a given mechanisms (use RDS, SSA, & pre-equilibrium)
15. Understand mechanisms are models or “stories” we tell. They are a hypothesis from which certain that we check to confirm is consistent with experimental rate laws
16. Identify chain reactions and branched chain reaction mechanism and their elementary steps (initiation, propagation, termination)

### Arrhenius Law (Temperature dependance)

1. Low activation energy  = high rates and low temperature sensitivity
2. High activation energies  = low rate and high temperature sensitivity
3. Know what a reasonable activation energy is for a reaction that is making and breaking chemical bonds in units of kJ/mol
4. Increase a typical reaction by 10 ºC yields roughly a doubling in the rate of reaction.
5. Define and understand the molecular rational for the activation energy (Ea) and pre-exponential factor (A)

### Reaction coordinate diagrams

1. Interpret it's representation of a chemical reaction mechanism
2. Label axes ($\Delta G$ and rxn coord)
3. Locate reactants and products
4. Label $\Delta G_{rxn}$
5. Label activation energy
6. Draw the structure of the activated complex
7. Label the transition state
8. Label intermediate states
9. Count the number of elementary steps given a reaction coordinate diagram
10. Count the number of intermediates given a reaction coordinate diagram
11. Apply Hammond's Postulate relating thermodynamics to the geometric structure of the activated complex, and its relative position on a reaction coordinate diagrams. 

### Collision theory

1. Understand the microscopic derivation of $k$ using statistical mechanics
3. Understand the qualitative role collision frequency has on rate of reaction
4. Explain collision cross section and compare effective collision cross sections between different reactants
5. Explain what the mean speed of reactants impacts the rate of reaction
6. Apply the dependance of rates on kinetic energy, temperature, and speed to predict if the rate of reaction will increase or decrease
7. Why is the steric factor needed in the collision theory to accurately predict the rate of reaction?
8. Understand the relationship between collision theory's $E_{min}$ and the Arrhenius activation energy ($E_A$)
9. In an elementary reaction step if 1 bond is broken and 1 bond is formed the activation energy is roughly 8% the bond energy
10. In an elementary reaction step if 2 bonds are broken and 2 bonds are formed the activation energy is roughly 30% the sum of the bond energies for the bonds that were broken.
11. Sterics (complicated but we’ll leave it as very bulks compounds often react more slowly than smaller reactants)

### Transition state theory

1. Define transition state / activate complex and be able to identify it on a reaction coordinate diagram
2. Draw an endogonic/exergonic reaction coordinate diagram
3. Understand the relationship between equilibrium constant and the rate constant for the forwards and reverse reaction
4. Apply a reaction coordinate diagram to explain why the reverse reaction is always slower for an exergonic reaction
5. Apply a reaction coordinate diagram to explain why the reverse reaction is always faster for an endergonic reaction 
6. Reaction coordinate diagrams: relations between 𝛥G, Ea forward, Ea reverse, kf, kr, Keq, reactants, products, reactive intermediates, RDS, would you need SSA to write the rate law?

### Catalysis

1.  Identity a catalyst in a reaction mechanism
2.  Identify a heterogeneous catalyst
3.  Identify a homogenous catalyst
4.  Understand the mechanisms for how poisons can deactivate catalysts
5.  Understand and interpret the structures enzymes and how they participate in catalysis
6.  Apply Michaelis-Menten Kinetics and the qualitative dependance of a reaction rate on the concentration of substrate and enzyme. 
7.  Draw rxn coordinate diagrams for catalytic reactions and compare them to uncatalyzed reactions