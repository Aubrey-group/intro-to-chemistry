# Measuring rates of reaction

$$
rate = \frac{\Delta [A]}{\Delta t}
$$